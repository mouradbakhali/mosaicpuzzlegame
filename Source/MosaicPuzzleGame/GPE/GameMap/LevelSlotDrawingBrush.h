// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelSlotDrawingBrush.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API ALevelSlotDrawingBrush : public AActor
{
	GENERATED_BODY()
		
	// Functions /////////////////////////////////////
public:
	ALevelSlotDrawingBrush(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void ChangeBrushColor(FLinearColor NewColor);

	UFUNCTION(BlueprintImplementableEvent, Category = "DrawingBrushEvents")
	void OnChangingDrawingColor(FLinearColor Color);

	// Variables /////////////////////////////////////
private:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBillboardComponent* BillbordComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* RenderComponent;
	
};
