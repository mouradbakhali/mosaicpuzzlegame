// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Common/GameStructures.h"
#include "LevelSlot.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API ALevelSlot : public AActor
{
	GENERATED_BODY()
	
	// Functions /////////////////////////////////////
public:
	ALevelSlot(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void FillSlot(FDrawingColor Color);

	UFUNCTION()
	void ClearSlot();

	UFUNCTION()
	void CheckSlotFill();

	// Variables /////////////////////////////////////
private:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBillboardComponent* BillbordComp;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* RenderComponent;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBoxComponent* BoxComponent;
		
public:
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;

	UPROPERTY()
	int32 Index1D;

	UPROPERTY()
	int32 IndexCol;

	UPROPERTY()
	int32 IndexRow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Slot Fill")
	TSubclassOf<class ALevelSlotFill> LevelSlotFillBPClass;
	
	UPROPERTY()
	class ALevelSlotFill* LevelSlotFill;

	UPROPERTY(BlueprintReadOnly, Category = "Colors")
	FString FillColorName;
};
