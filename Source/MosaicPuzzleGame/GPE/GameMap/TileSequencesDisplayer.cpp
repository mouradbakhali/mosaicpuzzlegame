// Game created by Bakhali. All right reserved.

#include "TileSequencesDisplayer.h"
#include "DrawDebugHelpers.h"
#include "GPE/GameMap/LevelSlot.h"
#include "Engine/CollisionProfile.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"
#include "Classes/Components/BillboardComponent.h"
#include "Classes/Components/TextRenderComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
ATileSequencesDisplayer::ATileSequencesDisplayer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BillbordComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billbord Component"));
	RootComponent = BillbordComp;

	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));
	RenderComponent->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	RenderComponent->Mobility = EComponentMobility::Movable;
	RenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);

}

// Called when the game starts or when spawned
void ATileSequencesDisplayer::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ATileSequencesDisplayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATileSequencesDisplayer::UpdateLevelSlotsSequences(TArray<ALevelSlot*> List, FString ColorID, bool bIsCol)
{
	// Set LevelSlotsSequenceList
	LevelSlotsSequenceList = List;

	TilesSequencesList.Empty();

	// Set TilesSequencesList
	int32 SequencesCounter = 0;
	for (int32 i = 0; i < LevelSlotsSequenceList.Num(); i++)
	{
		if (LevelSlotsSequenceList[i]->FillColorName == "ColorID")
		{		
			// Full tile
			SequencesCounter++;
			if (i == LevelSlotsSequenceList.Num() - 1)
			{
				TilesSequencesList.Add(SequencesCounter);
			}
		}
		else
		{
			// Empty tile
			if (SequencesCounter > 0)
			{
				TilesSequencesList.Add(SequencesCounter);
				SequencesCounter = 0;
			}
		}
	}

	// Create and set text renderer components	 
	this->bIsCol = bIsCol;
	float TextOffset = 25.0f;
	float TextSpacing = 40.0f;
	for (int i = 0; i < TilesSequencesList.Num(); i++)
	{
		if (i > TextRenderComponentList.Num() - 1)
			TextRenderComponentList.Add(CreateTextRenderComponent());

		FVector TextLocation= FVector::FVector(0.0f, -(TextOffset + (i * TextSpacing)), 5.0f);
		if (bIsCol)
		{
			TextRenderComponentList[i]->SetWorldRotation(FRotator::FRotator(90.0f, 90.0f, 0.0f));
		}
		else
		{
			TextRenderComponentList[i]->SetWorldRotation(FRotator::FRotator(90.0f, 0.0f, -90.0f));
		}

		TextRenderComponentList[i]->SetText(FString::Printf(TEXT("%d"), TilesSequencesList[i]));
		TextRenderComponentList[i]->SetRelativeLocation(TextLocation);
		TextRenderComponentList[i]->SetWorldScale3D(FVector::OneVector *1.8f);
		TextRenderComponentList[i]->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
		TextRenderComponentList[i]->SetVerticalAlignment(EVerticalTextAligment::EVRTA_TextCenter);
		if (TextMaterial)
		{
			TextRenderComponentList[i]->SetMaterial(0, TextMaterial);
			UMaterialInstanceDynamic* MatDyn = TextRenderComponentList[i]->CreateDynamicMaterialInstance(0, TextRenderComponentList[i]->GetMaterial(0));
			if (MatDyn)
			{
				MatDyn->SetVectorParameterValue("Color", TextColor);
			}
		}
	}
}

void ATileSequencesDisplayer::UpdateLevelSlotsSequencesOnly(FString ColorID)
{
	// Empty TilesSequencesList
	TilesSequencesList.Empty();

	// Set TilesSequencesList
	int32 SequencesCounter = 0;
	for (int32 i = 0; i < LevelSlotsSequenceList.Num(); i++)
	{
		UE_LOG(LogTemp, Log, TEXT("Name = %s | i = %d: FillColorName = %s, ColorID = %s"), *GetName(), i, *LevelSlotsSequenceList[i]->FillColorName, *ColorID);
		if (LevelSlotsSequenceList[i]->FillColorName == ColorID)
		{
			// Full tile
			SequencesCounter++;
			if (i == LevelSlotsSequenceList.Num() - 1)
			{
				TilesSequencesList.Add(SequencesCounter);
			}
		}
		else
		{
			// Empty tile
			if (SequencesCounter > 0)
			{
				TilesSequencesList.Add(SequencesCounter);
				SequencesCounter = 0;
			}
		}
	}

	// Create and set text renderer components	 
	float TextOffset = 25.0f;
	float TextSpacing = 40.0f;

	// Hide all text
	for (int i = 0; i < TextRenderComponentList.Num(); i++)
	{
		TextRenderComponentList[i]->SetVisibility(false);
	}

	// Init text to display tiles sequences
	for (int i = 0; i < TilesSequencesList.Num(); i++)
	{
		if (i > TextRenderComponentList.Num() - 1)
			TextRenderComponentList.Add(CreateTextRenderComponent());

		FVector TextLocation = FVector::FVector(0.0f, -(TextOffset + (i * TextSpacing)), 5.0f);
		if (bIsCol)
		{
			TextRenderComponentList[i]->SetWorldRotation(FRotator::FRotator(90.0f, 90.0f, 0.0f));
		}
		else
		{
			TextRenderComponentList[i]->SetWorldRotation(FRotator::FRotator(90.0f, 0.0f, -90.0f));
		}


		TextRenderComponentList[i]->SetVisibility(true);
		TextRenderComponentList[i]->SetText(FString::Printf(TEXT("%d"), TilesSequencesList[i]));
		TextRenderComponentList[i]->SetRelativeLocation(TextLocation);
		TextRenderComponentList[i]->SetWorldScale3D(FVector::OneVector *1.8f);
		TextRenderComponentList[i]->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
		TextRenderComponentList[i]->SetVerticalAlignment(EVerticalTextAligment::EVRTA_TextCenter);
	}
}

UTextRenderComponent* ATileSequencesDisplayer::CreateTextRenderComponent()
{
	UTextRenderComponent* TextRenderComponent = NewObject<UTextRenderComponent>(this);
	
	if (!TextRenderComponent) { return nullptr; }
	TextRenderComponent->RegisterComponent();

	TextRenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);
	
	return TextRenderComponent;
}
