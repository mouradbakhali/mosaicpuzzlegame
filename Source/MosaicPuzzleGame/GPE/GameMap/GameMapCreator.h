// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameManagers/MainGameInstance.h"
#include "Common/GameStructures.h"
#include "GameMapCreator.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API AGameMapCreator : public AActor
{
	GENERATED_BODY()
	

	// Functions ///////////////////////////////////////////
public:	
	AGameMapCreator();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	// Change level size
	UFUNCTION()
	bool AddLevelCol();
	UFUNCTION()
	bool SubLevelCol();
	UFUNCTION()
	bool AddLevelRow();
	UFUNCTION()
	bool SubLevelRow();

	UFUNCTION()
	void UpdateLevelDrawer();

	UFUNCTION()
	void UpdatesLevelSlotFills();

	UFUNCTION()
	void UpdateStructColorList();

	UFUNCTION()
	void UpdateLevelSlotBrushLocation(class ALevelSlot* LevelSlot, FVector MouseWorldLocation);

	UFUNCTION()
	void UpdateLevelDrawerforLoadedLevel();

	void DestroyAllSlotFills();

	void UpdateSlotFills();

	UFUNCTION()
	void ChangeDrawingColor(FDrawingColor InNewColor);

	UFUNCTION()
	void InitializeLevelDatasStruct();

	UFUNCTION()
	void AddNewLocalLevelForEdit(FString NewLevelName);

	UFUNCTION()
	void AddNewOnlineLevelForEdit();

	UFUNCTION()
	void LoadSelectedLocalLevelForEdit(FString LevelName);

	UFUNCTION()
	void LoadSelectedOnlineLevelForEdit(FString LevelName);

	UFUNCTION()
	void SaveSelectedLevel();

	UFUNCTION()
	void ClearSelectedLevel();

	UFUNCTION()
	void SaveLevelLocally(FString LevelName);

	UFUNCTION()
	void SaveLevelOnline();

	UFUNCTION()
	void LoadLevelLocally(FString LevelName, FLevelDatas& LevelDatas);

	UFUNCTION()
	void LoadLevelOnline();

	// Variables ///////////////////////////////////////////
public:
	UPROPERTY()
	class UMainGameInstance* GameInstance;
	
	UPROPERTY()
	class ALevelDrawer* LevelDrawer;

	UPROPERTY()
	class AMainPlayer* MainPlayer;

	UPROPERTY()
	FLevelDatas LevelDatas;

	UPROPERTY()
	float SlotSize = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drawing Brush")
	class ALevelSlotDrawingBrush* LevelSlotDrawingBrush;

	UPROPERTY()
	FString SelectedForEditLevelName = "";

	UPROPERTY()
	ELevelEditType LevelEditType;
};
