// Game created by Bakhali. All right reserved.

#include "GameMapCreator.h"
#include "GameManagers/MainGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "LevelDrawer.h"
#include "Player/MainPlayer.h"
#include "LevelSlot.h"
#include "LevelSlotFill.h"
#include "LevelSlotDrawingBrush.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/JsonUtilities/Public/JsonUtilities.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "Runtime/JsonUtilities/Public/JsonObjectWrapper.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Core/Public/Misc/Paths.h"
#include "Core/Public/Misc/Paths.h"
#include "Core/Public/HAL/FileManagerGeneric.h"
#include "Common/GameStructures.h"


AGameMapCreator::AGameMapCreator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AGameMapCreator::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AGameMapCreator > BeginPlay > GameInstance is nullptr"));

	}

	// Initialize selected color
	GameInstance->Initialize();

	// Set pointer to this instance of game map creator
	GameInstance->GameMapCreator = this;

	// Initialize level drawer
	TArray<AActor*> LevelDrawerActors;
	UGameplayStatics::GetAllActorsOfClass(this, ALevelDrawer::StaticClass(), LevelDrawerActors);
	if (LevelDrawerActors.Num() > 0)
	{
		LevelDrawer = Cast<ALevelDrawer>(LevelDrawerActors[0]);
		if (LevelDrawerActors.Num() > 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("AGameMapCreator > BeginPlay > More than one LevelDrawer in the level !"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > BeginPlay > No LevelDrawer in the level !"));
	}

	// Initialize main player
	MainPlayer = Cast<AMainPlayer>(UGameplayStatics::GetPlayerPawn(this, 0));
	MainPlayer->GameMapCreator = this;

	// Create test level 
	LevelDatas.LevelID = 11;
	LevelDatas.LevelGridColNum = 5;
	LevelDatas.LevelGridRowNum = 5;
	LevelDatas.SlotSize = 100.0f;

	// Read level ID from game instance

	// Draw level map > Level Drawer
	LevelDrawer->DrawLevelSlots(LevelDatas);
	LevelDrawer->GameMapCreator = this;

	// Update camera position
	MainPlayer->UpdateCameraPositionAndSize(LevelDatas);

	// Set level edit type
	LevelEditType = ELevelEditType::ONLINE;
}

void AGameMapCreator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Change levels slot size
bool AGameMapCreator::AddLevelCol()
{
	LevelDatas.LevelGridColNum++;
	bool IsNotReachingLimit = true;

	if (LevelDatas.LevelGridColNum > GameInstance->LevelMaxSizeX)
	{
		LevelDatas.LevelGridColNum = GameInstance->LevelMaxSizeX;
		IsNotReachingLimit= false;
	}
	UpdateLevelDrawer();
	return IsNotReachingLimit;
}
bool AGameMapCreator::SubLevelCol()
{
	LevelDatas.LevelGridColNum--;
	bool IsNotReachingLimit = true;

	if (LevelDatas.LevelGridColNum < GameInstance->LevelMinSizeX)
	{
		LevelDatas.LevelGridColNum = GameInstance->LevelMinSizeX;
		IsNotReachingLimit = false;
	}
	UpdateLevelDrawer();
	return IsNotReachingLimit;
}
bool AGameMapCreator::AddLevelRow()
{
	LevelDatas.LevelGridRowNum++;
	bool IsNotReachingLimit = true;

	if (LevelDatas.LevelGridRowNum > GameInstance->LevelMaxSizeY)
	{
		LevelDatas.LevelGridRowNum = GameInstance->LevelMaxSizeY;
		IsNotReachingLimit = false;
	}
	UpdateLevelDrawer();
	return IsNotReachingLimit;
}
bool AGameMapCreator::SubLevelRow()
{
	LevelDatas.LevelGridRowNum--;
	bool IsNotReachingLimit = true;

	if (LevelDatas.LevelGridRowNum < GameInstance->LevelMinSizeY)
	{
		LevelDatas.LevelGridRowNum = GameInstance->LevelMinSizeY;
		IsNotReachingLimit = false;
	}
	UpdateLevelDrawer();
	return IsNotReachingLimit;
}

// Update levels slots and colors fill
void AGameMapCreator::UpdateLevelDrawer()
{
	// Draw level slots
	LevelDrawer->UpdateLevelSlots(LevelDatas);

	// Update level slot fills
	UpdatesLevelSlotFills();

	// Update struct color list
	UpdateStructColorList();

	// Update camera position
	MainPlayer->UpdateCameraPositionAndSize(LevelDatas);
}
void AGameMapCreator::UpdatesLevelSlotFills()
{
	TArray<AActor*> LevelSlotFillList;
	UGameplayStatics::GetAllActorsOfClass(this, ALevelSlotFill::StaticClass(), LevelSlotFillList);

	for (int i = 0; i < LevelSlotFillList.Num(); i++)
	{
		ALevelSlot* Slot = Cast<ALevelSlotFill>(LevelSlotFillList[i])->CheckSlotBelow();
		if (Slot)
		{
			if (Slot->LevelSlotFill != LevelSlotFillList[i])
				Slot->LevelSlotFill = Cast<ALevelSlotFill>(LevelSlotFillList[i]);
		}
		else
		{
			LevelSlotFillList[i]->Destroy();
		}
	}
}
void AGameMapCreator::UpdateStructColorList()
{
	LevelDatas.ColorsList.Empty();
	for (int i = 0; i < LevelDatas.LevelGridColNum * LevelDatas.LevelGridRowNum; i++)
	{
		if (LevelDrawer->LevelSlotsList[i]->LevelSlotFill != nullptr)
		{
			LevelDatas.ColorsList.Add(LevelDrawer->LevelSlotsList[i]->LevelSlotFill->FillColor);
		}
		else
		{
			LevelDatas.ColorsList.Add(FDrawingColor::FDrawingColor());
		}
	}
}
void AGameMapCreator::UpdateLevelSlotBrushLocation(ALevelSlot * LevelSlot, FVector MouseWorldLocation)
{
	FVector SlotBrushLoc;
	FVector BrushLocalScale = FVector::OneVector;
	if (LevelSlot)
	{
		SlotBrushLoc = LevelSlot->GetActorLocation();
		SlotBrushLoc.Z = 20.0f;
		
	}
	else
	{
		SlotBrushLoc = MouseWorldLocation;
		SlotBrushLoc.Z = 20.0f;
		BrushLocalScale *= 0.5f;
	}

	if (LevelSlotDrawingBrush)
	{
		LevelSlotDrawingBrush->SetActorLocation(SlotBrushLoc);
		LevelSlotDrawingBrush->SetActorScale3D(BrushLocalScale);
	}
}
void AGameMapCreator::UpdateLevelDrawerforLoadedLevel()
{
	// Draw level slots
	LevelDrawer->UpdateLevelSlots(LevelDatas);

	// Destroy all slot fills
	DestroyAllSlotFills();
	
	// Draw slots fill
	UpdateSlotFills();

	// Update camera position
	MainPlayer->UpdateCameraPositionAndSize(LevelDatas);
}
void AGameMapCreator::DestroyAllSlotFills()
{
	TArray<AActor*> LevelSlotFillList;
	UGameplayStatics::GetAllActorsOfClass(this, ALevelSlotFill::StaticClass(), LevelSlotFillList);

	for (int i = 0; i < LevelSlotFillList.Num(); i++)
	{
		LevelSlotFillList[i]->Destroy();
	}
}
void AGameMapCreator::UpdateSlotFills()
{
	for (int i = 0; i < LevelDatas.ColorsList.Num(); i++)
	{
		if (LevelDatas.ColorsList[i].ColorID != "")
		{
			if (LevelDrawer->LevelSlotsList[i]->LevelSlotFill != nullptr)
			{
				LevelDrawer->LevelSlotsList[i]->LevelSlotFill->Destroy();
			}
			LevelDrawer->LevelSlotsList[i]->FillSlot(LevelDatas.ColorsList[i]);
		}
	}
}
void AGameMapCreator::ChangeDrawingColor(FDrawingColor InNewColor)
{
	LevelSlotDrawingBrush->ChangeBrushColor(InNewColor.Color);
}
void AGameMapCreator::InitializeLevelDatasStruct()
{
	LevelDatas = FLevelDatas::FLevelDatas();
	LevelDatas.ColorsList.Empty();
	for (int i = 0; i < LevelDatas.LevelGridColNum * LevelDatas.LevelGridRowNum; i++)
	{
		LevelDatas.ColorsList.Add(FDrawingColor::FDrawingColor());
	}
}

// Local & online level add/edit
void AGameMapCreator::AddNewLocalLevelForEdit(FString NewLocalLevelName)
{
	LevelEditType = ELevelEditType::LOCAL;
	SelectedForEditLevelName = NewLocalLevelName;
	InitializeLevelDatasStruct(); 
	SaveLevelLocally(SelectedForEditLevelName);
	LoadLevelLocally(SelectedForEditLevelName, LevelDatas);
	UpdateLevelDrawerforLoadedLevel();
}
void AGameMapCreator::AddNewOnlineLevelForEdit()
{

}
void AGameMapCreator::LoadSelectedLocalLevelForEdit(FString LevelName)
{
	LevelEditType = ELevelEditType::LOCAL;
	SelectedForEditLevelName = LevelName;
	LoadLevelLocally(LevelName, LevelDatas);
	UpdateLevelDrawerforLoadedLevel();
}
void AGameMapCreator::LoadSelectedOnlineLevelForEdit(FString LevelName)
{
}

// Save & clear selected level
void AGameMapCreator::SaveSelectedLevel()
{
	switch (LevelEditType)
	{
	case ELevelEditType::LOCAL:
		UpdateStructColorList();
		SaveLevelLocally(SelectedForEditLevelName);
		break;
	case ELevelEditType::ONLINE:
		break;
	}
}
void AGameMapCreator::ClearSelectedLevel()
{
	InitializeLevelDatasStruct();
	UpdateLevelDrawerforLoadedLevel();
}

// Save & load functions
void AGameMapCreator::SaveLevelLocally(FString LevelName)
{
	// Fill level data infos
	//UpdateStructColorList();

	// Create save data object
	TSharedPtr<FJsonObject> SavedJSonObject;
	SavedJSonObject = MakeShareable(new FJsonObject());

	// Convert level's struct to json
	SavedJSonObject = FJsonObjectConverter::UStructToJsonObject(LevelDatas);
	
	// Convert save game json object to string
	FString JSonToSaveString;

	// Writer
	TSharedRef< TJsonWriter<> > JsonWriter = TJsonWriterFactory<>::Create(&JSonToSaveString);
	FJsonSerializer::Serialize(SavedJSonObject.ToSharedRef(), JsonWriter);
	
	// Generate level path
	FString LevelPath = GameInstance->GetLocalLevelsFolderPath() + LevelName;

	// Save level's file
	FFileHelper::SaveStringToFile(*JSonToSaveString, *LevelPath);
}
void AGameMapCreator::SaveLevelOnline()
{}
void AGameMapCreator::LoadLevelLocally(FString LevelName, FLevelDatas& LevelDatas)
{
	// Create save data object
	TSharedPtr<FJsonObject> LoadedDataJSonObject;
	LoadedDataJSonObject = MakeShareable(new FJsonObject());

	// Generate path
	FString LevelPath = GameInstance->GetLocalLevelsFolderPath() + LevelName;

	// Load file to string
	FString LoadedJSonString = TEXT("");
	FFileHelper::LoadFileToString(LoadedJSonString, *LevelPath);

	// Deseralize
	TSharedRef< TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(*LoadedJSonString);
	FJsonSerializer::Deserialize(JsonReader, LoadedDataJSonObject);

	// Convert level's json to struct
	FJsonObjectConverter::JsonObjectStringToUStruct<FLevelDatas>(LoadedJSonString, &LevelDatas, 0, 0);	
}
void AGameMapCreator::LoadLevelOnline()
{}