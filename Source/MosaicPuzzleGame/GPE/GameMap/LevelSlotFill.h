// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Common/GameStructures.h"
#include "LevelSlotFill.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API ALevelSlotFill : public AActor
{
	GENERATED_BODY()
	
	// Functions /////////////////////////////////////
public:
	ALevelSlotFill(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void ChangeFillColor(FDrawingColor NewColor);

	class ALevelSlot* CheckSlotBelow();

	UFUNCTION(BlueprintImplementableEvent, Category = "SlotFillEvents")
	void OnChangingFillColor(FLinearColor Color);

	// Variables /////////////////////////////////////
private:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBillboardComponent* BillbordComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* RenderComponent;
public:
	UPROPERTY()
	FDrawingColor FillColor;
};
