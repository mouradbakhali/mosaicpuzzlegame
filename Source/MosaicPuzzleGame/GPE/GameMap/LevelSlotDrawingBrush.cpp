// Game created by Bakhali. All right reserved.

#include "LevelSlotDrawingBrush.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"
#include "GameManagers/MainGameInstance.h"
#include "Engine/CollisionProfile.h"
#include "Classes/Components/BillboardComponent.h"

// Sets default values
ALevelSlotDrawingBrush::ALevelSlotDrawingBrush(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	BillbordComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billbord Component"));
	RootComponent = BillbordComp;

	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));
	RenderComponent->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	RenderComponent->Mobility = EComponentMobility::Movable;
	RenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);
}

// Called when the game starts or when spawned
void ALevelSlotDrawingBrush::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelSlotDrawingBrush::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelSlotDrawingBrush::ChangeBrushColor(FLinearColor NewColor)
{
	OnChangingDrawingColor(NewColor);
}