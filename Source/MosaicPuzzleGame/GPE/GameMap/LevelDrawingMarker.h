// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Common/GameStructures.h"
#include "LevelDrawingMarker.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API ALevelDrawingMarker : public AActor
{
	GENERATED_BODY()
	
	// Functions /////////////////////////////////////
public:
	// Sets default values for this actor's properties
	ALevelDrawingMarker(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void UpdateLevelDrawingMarker(FVector MarkerLocation);

	UFUNCTION()
	void InitilizeSlotMarker(FLevelDatas LevelDatas);

	// Variables /////////////////////////////////////
private:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBillboardComponent* BillbordComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* HorLineRenderComponent;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* VertLineRenderComponent;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* RectMarkerRenderComponent;
public:
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;

	UPROPERTY()
	FVector LinesCenterLocation;
};
