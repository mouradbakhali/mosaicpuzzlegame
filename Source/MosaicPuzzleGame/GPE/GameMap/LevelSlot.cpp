// Game created by Bakhali. All right reserved.

#include "LevelSlot.h"
#include "Engine/CollisionProfile.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"
#include "GameManagers/MainGameInstance.h"
#include "Classes/Components/BillboardComponent.h"
#include "Classes/Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Gpe/GameMap/LevelSlotFill.h"
#include "DrawDebugHelpers.h"

// Sets default values
ALevelSlot::ALevelSlot(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BillbordComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billbord Component"));
	RootComponent = BillbordComp;

	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));
	RenderComponent->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	RenderComponent->Mobility = EComponentMobility::Movable;
	RenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);

	// Create tag
	if (Tags.Num() == 0)
		Tags.Add(FName(TEXT("LEVEL_SLOT")));
	else if (Tags.Num() > 1)
		Tags.RemoveAt(1);

}

// Called when the game starts or when spawned
void ALevelSlot::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelEditorUI > NativeConstruct > GameInstance is nullptr"));
	}
}

// Called every frame
void ALevelSlot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALevelSlot::FillSlot(FDrawingColor Color)
{
	// Spawn fill slot
	FVector SpawnLoc = GetActorLocation() + FVector(0.0f, 0.0f, 10.0f);
	LevelSlotFill = GetWorld()->SpawnActor<ALevelSlotFill>(LevelSlotFillBPClass, SpawnLoc, FRotator::ZeroRotator);
	LevelSlotFill->ChangeFillColor(Color);
}

void ALevelSlot::ClearSlot()
{
	if (LevelSlotFill != nullptr)
	{
		LevelSlotFill->Destroy();
	}
}

void ALevelSlot::CheckSlotFill()
{

	// Cast ray to slots
	EDrawDebugTrace::Type DrawDebugType;
	DrawDebugType = EDrawDebugTrace::ForOneFrame;

	FVector StartLoc = GetActorLocation() - (FVector::UpVector * 10.0f);
	FVector EndLoc = StartLoc + (-FVector::UpVector * 700.0f);
	FHitResult Hit;

	// Actors to ignore
	TArray<AActor*> ActorToIgnore;
	ActorToIgnore.Add(this);

	// Do trace
	UKismetSystemLibrary::LineTraceSingle(this, StartLoc, EndLoc, UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_WorldDynamic), false, ActorToIgnore, DrawDebugType, Hit, true);

	// Check if we have an appropriate location
	if (Hit.bBlockingHit && Hit.Actor != nullptr && Hit.Actor->ActorHasTag(FName(TEXT("LEVEL_SLOT"))))
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), 20.0f, 12, FColor::Green, true, 1.0f);

	}
	else
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), 20.0f, 12, FColor::Red, true, 1.0f);
	}
}

