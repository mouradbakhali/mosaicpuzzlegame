// Game created by Bakhali. All right reserved.

#include "LevelDrawer.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "LevelSlot.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "GPE/GameMap/TileSequencesDisplayer.h"
#include "DrawDebugHelpers.h"

// Sets default values
ALevelDrawer::ALevelDrawer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ALevelDrawer::BeginPlay()
{
	Super::BeginPlay();

}

void ALevelDrawer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelDrawer::DrawLevelSlots(FLevelDatas InLevelDatas)
{
	int GridColNum = InLevelDatas.LevelGridColNum;
	int GridRowNum = InLevelDatas.LevelGridRowNum;

	if (LevelSlotBPClass != nullptr)
	{
		int TotalSlots = GridColNum * GridRowNum;
		for (int i = 0; i < TotalSlots; i++)
		{
			int32 IndexCol = (i % GridColNum);
			int32 IndexRow = (i / GridColNum);
			
			FVector SpawnLoc = FVector(IndexCol * 100.0f, IndexRow * 100.0f, 0.0f);			
			ALevelSlot* LevelSlot = GetWorld()->SpawnActor<ALevelSlot>(LevelSlotBPClass, SpawnLoc, FRotator::ZeroRotator);
			
			LevelSlot->IndexCol = IndexCol;
			LevelSlot->IndexRow = IndexRow;
			LevelSlot->Index1D = i;
			//LevelSlot->SetActorLabel(*FString::Printf(TEXT("BP_LevelSlot_%d_%d"), IndexCol, IndexRow), false);

			LevelSlotsList.Add(LevelSlot);
		}
		CurrentGridColNum = GridColNum;
		CurrentGridRowNum = GridRowNum;
	}
}


void ALevelDrawer::UpdateLevelSlots(FLevelDatas InLevelDatas)
{
	// Store temporary slot fill

	// Clear and destroy slots list
	for (int i = 0; i < LevelSlotsList.Num(); i++)
	{
		LevelSlotsList[i]->Destroy();
	}
	LevelSlotsList.Empty();

	// Draw level
	DrawLevelSlots(InLevelDatas);
}

void ALevelDrawer::DrawLevelSlotsForPlay(FLevelDatas InLevelDatas, FString SelectColorID)
{
	int GridColNum = InLevelDatas.LevelGridColNum;
	int GridRowNum = InLevelDatas.LevelGridRowNum;

	if (LevelSlotBPClass != nullptr)
	{
		int TotalSlots = GridColNum * GridRowNum;
		for (int i = 0; i < TotalSlots; i++)
		{
			int32 IndexCol = (i % GridColNum);
			int32 IndexRow = (i / GridColNum);

			FVector SpawnLoc = FVector(IndexCol * 100.0f, IndexRow * 100.0f, 0.0f);
			ALevelSlot* LevelSlot = GetWorld()->SpawnActor<ALevelSlot>(LevelSlotGameplayBPClass, SpawnLoc, FRotator::ZeroRotator);

			LevelSlot->IndexCol = IndexCol;
			LevelSlot->IndexRow = IndexRow;
			LevelSlot->Index1D = i;
			LevelSlot->FillColorName = InLevelDatas.ColorsList[i].ColorID;

			LevelSlotsList.Add(LevelSlot);
		}
		CurrentGridColNum = GridColNum;
		CurrentGridRowNum = GridRowNum;
	}

	DrawTilesSequencesDisplayer(InLevelDatas, SelectColorID);
}

void ALevelDrawer::DrawTilesSequencesDisplayer(FLevelDatas InLevelDatas, FString SelectColorID)
{
	int GridColNum = InLevelDatas.LevelGridColNum;
	int GridRowNum = InLevelDatas.LevelGridRowNum;
	float SlotSize = 100.0f;

	// Handle cols 
	for (int32 i = 0; i < GridColNum; i++)
	{
		FVector ColLoc = FVector(100.0f * i, -(SlotSize / 2.0f), -5.0f);

		ATileSequencesDisplayer* TileSequencesDisplayerCol = GetWorld()->SpawnActor<ATileSequencesDisplayer>(TileSequencesDisplayerBPClass, ColLoc, FRotator::ZeroRotator);
		TArray<ALevelSlot*> TempSlotsSequenceList;
		for (int32 j = 0; j < GridRowNum; j++)
		{

			int32 Index = j * GridColNum + i;
			TempSlotsSequenceList.Add(LevelSlotsList[Index]);

		}	
		TileSequencesDisplayersList.Add(TileSequencesDisplayerCol);
		TileSequencesDisplayerCol->UpdateLevelSlotsSequences(TempSlotsSequenceList, SelectColorID);
	}

	// Handle rows
	for (int32 i = 0; i < GridRowNum; i++)
	{
		FVector RowLoc = FVector(-(SlotSize / 2.0f), 100.0f * i, -5.0f);

		ATileSequencesDisplayer* TileSequencesDisplayerRow = GetWorld()->SpawnActor<ATileSequencesDisplayer>(TileSequencesDisplayerBPClass, RowLoc, FRotator::FRotator(0.0f, -90.0f, 0.0f));

		TArray<ALevelSlot*> TempSlotsSequenceList;
		for (int32 j = 0; j < GridColNum; j++)
		{

			int32 Index = i * GridColNum + j;
			UE_LOG(LogTemp, Log, TEXT("Index = %d | LevelSlotsList number = %d"), Index, LevelSlotsList.Num());
			if (Index < LevelSlotsList.Num())
				TempSlotsSequenceList.Add(LevelSlotsList[Index]);
			else
				UE_LOG(LogTemp, Log, TEXT("Out of array size > Index = %d | LevelSlotsList number = %d"), Index, LevelSlotsList.Num());

		}

		TileSequencesDisplayersList.Add(TileSequencesDisplayerRow);
		TileSequencesDisplayerRow->UpdateLevelSlotsSequences(TempSlotsSequenceList, SelectColorID, false);
	}
}

void ALevelDrawer::UpdateTilesSequencesDisplayer(FString ColorID)
{
	if (TileSequencesDisplayersList.Num() == 0)
		return;

	for (int i = 0; i < TileSequencesDisplayersList.Num(); i++)
	{
		TileSequencesDisplayersList[i]->UpdateLevelSlotsSequencesOnly(ColorID);
	}
}