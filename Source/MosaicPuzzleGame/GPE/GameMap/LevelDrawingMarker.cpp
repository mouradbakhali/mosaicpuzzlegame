// Game created by Bakhali. All right reserved.

#include "LevelDrawingMarker.h"
#include "Engine/CollisionProfile.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"
#include "GameManagers/MainGameInstance.h"
#include "Classes/Components/BillboardComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

ALevelDrawingMarker::ALevelDrawingMarker(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BillbordComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billbord Component"));
	RootComponent = BillbordComp;

	// Horizontal line
	HorLineRenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("HorLineRenderComponent"));
	HorLineRenderComponent->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	HorLineRenderComponent->Mobility = EComponentMobility::Movable;
	HorLineRenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	// Vertical line
	VertLineRenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("VertLineRenderComponent"));
	VertLineRenderComponent->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	VertLineRenderComponent->Mobility = EComponentMobility::Movable;
	VertLineRenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// RectMarkerRenderComponent
	RectMarkerRenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RectMarkerRenderComponent"));
	RectMarkerRenderComponent->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	RectMarkerRenderComponent->Mobility = EComponentMobility::Movable;
	RectMarkerRenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);


}

void ALevelDrawingMarker::BeginPlay()
{
	Super::BeginPlay();

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelEditorUI > NativeConstruct > GameInstance is nullptr"));
	}

	InitilizeSlotMarker(GameInstance->LevelDatas);
}

void ALevelDrawingMarker::InitilizeSlotMarker(FLevelDatas LevelDatas)
{
	HorLineRenderComponent->SetRelativeScale3D(FVector(LevelDatas.LevelGridColNum, 1.0f, 1.0f));
	VertLineRenderComponent->SetRelativeScale3D(FVector(1.0f, 1.0f, LevelDatas.LevelGridRowNum));
	float LocX = (LevelDatas.LevelGridColNum * LevelDatas.SlotSize) / 2.0f - LevelDatas.SlotSize/2.0f;
	float LocY = (LevelDatas.LevelGridRowNum * LevelDatas.SlotSize) / 2.0f - LevelDatas.SlotSize / 2.0f;
	float LocZ = 10.0f;
	LinesCenterLocation = FVector(LocX, LocY, LocY);
}

void ALevelDrawingMarker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelDrawingMarker::UpdateLevelDrawingMarker(FVector MarkerLocation)
{

	HorLineRenderComponent->SetWorldLocation(FVector(LinesCenterLocation.X, MarkerLocation.Y, 30.0f));
	VertLineRenderComponent->SetWorldLocation(FVector(MarkerLocation.X, LinesCenterLocation.Y, 40.0f));
	RectMarkerRenderComponent->SetWorldLocation(FVector(MarkerLocation.X, MarkerLocation.Y, 50.0f));
}