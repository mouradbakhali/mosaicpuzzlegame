// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TileSequencesDisplayer.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API ATileSequencesDisplayer : public AActor
{
	GENERATED_BODY()
	// Functions /////////////////////////////////////
public:
	ATileSequencesDisplayer();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	void UpdateLevelSlotsSequences(TArray<class ALevelSlot*> List, FString ColorID, bool bIsCol = true);

	UFUNCTION()
	void UpdateLevelSlotsSequencesOnly(FString ColorID);

	UFUNCTION()
	class UTextRenderComponent* CreateTextRenderComponent();

	// Variables /////////////////////////////////////
private:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UBillboardComponent* BillbordComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Sprite)
	class UPaperSpriteComponent* RenderComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Text)
	TArray<class UTextRenderComponent*> TextRenderComponentList;

public:
	UPROPERTY()
	TArray<class ALevelSlot*> LevelSlotsSequenceList;

	UPROPERTY(BlueprintReadOnly, Category = "TilesSequences")
	TArray<int32> TilesSequencesList;

	UPROPERTY(EditDefaultsOnly, AdvancedDisplay, BlueprintReadOnly, Category = "TilesSequences")
	class UMaterialInterface* TextMaterial;

	UPROPERTY(EditDefaultsOnly, AdvancedDisplay, BlueprintReadOnly, Category = "TilesSequences")
	FLinearColor TextColor;

	UPROPERTY()
	bool bIsCol = true;
};
