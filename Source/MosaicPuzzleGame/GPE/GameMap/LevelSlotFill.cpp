// Game created by Bakhali. All right reserved.

#include "LevelSlotFill.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"
#include "GameManagers/MainGameInstance.h"
#include "Engine/CollisionProfile.h"
#include "Classes/Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "LevelSlot.h"
#include "DrawDebugHelpers.h"
#include "GameManagers/MainGameInstance.h"

// Sets default values
ALevelSlotFill::ALevelSlotFill(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	BillbordComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billbord Component"));
	RootComponent = BillbordComp;

	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));
	RenderComponent->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	RenderComponent->Mobility = EComponentMobility::Movable;
	RenderComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);

}

// Called when the game starts or when spawned
void ALevelSlotFill::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelSlotFill::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelSlotFill::ChangeFillColor(FDrawingColor NewColor)
{
	FillColor.ColorID = NewColor.ColorID;
	FillColor.Color = NewColor.Color;
	OnChangingFillColor(NewColor.Color);
}

ALevelSlot* ALevelSlotFill::CheckSlotBelow()
{
	// Cast ray to slots
	EDrawDebugTrace::Type DrawDebugType;
	DrawDebugType = EDrawDebugTrace::ForOneFrame;

	FVector StartLoc = GetActorLocation() - (FVector::UpVector * 10.0f);
	FVector EndLoc = StartLoc + (-FVector::UpVector * 700.0f);
	FHitResult Hit;

	// Actors to ignore
	TArray<AActor*> ActorToIgnore;
	ActorToIgnore.Add(this);

	// Do trace
	UKismetSystemLibrary::LineTraceSingle(this, StartLoc, EndLoc, UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_WorldDynamic), false, ActorToIgnore, DrawDebugType, Hit, true);
	
	// Check if we have an appropriate location
	if (Hit.bBlockingHit && Hit.Actor != nullptr && Hit.Actor->ActorHasTag(FName(TEXT("LEVEL_SLOT"))))
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), 20.0f, 12, FColor::Green, true, 1.0f);

		return Cast<ALevelSlot>(Hit.Actor);
	}
	else
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), 20.0f, 12, FColor::Red, true, 1.0f);
		return nullptr;
	}
}