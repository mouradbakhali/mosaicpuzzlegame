// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Common/GameStructures.h"
#include "LevelDrawer.generated.h"

/**
* This actor draw level slots based on SizeX and Size Y
* It also took care of updating the slots when editing a level
*/

UCLASS()
class MOSAICPUZZLEGAME_API ALevelDrawer : public AActor
{
	GENERATED_BODY()
	
	// Functions /////////////////////////////////////
public:	
	ALevelDrawer();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	// Game level editor functions
	UFUNCTION()
	void DrawLevelSlots(FLevelDatas InLevelDatas);

	UFUNCTION()
	void UpdateLevelSlots(FLevelDatas InLevelDatas);

	// Gameplay functions
	UFUNCTION()
	void DrawLevelSlotsForPlay(FLevelDatas InLevelDatas, FString SelectColorID);

	UFUNCTION()
	void DrawTilesSequencesDisplayer(FLevelDatas LevelDatas, FString SelectColorID);

	UFUNCTION()
	void UpdateTilesSequencesDisplayer(FString ColorID);

	// Variables /////////////////////////////////////
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = LevelSlots)
	TSubclassOf<class ALevelSlot> LevelSlotBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = LevelSlots)
	TSubclassOf<class ALevelSlot> LevelSlotGameplayBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "TileSequences")
	TSubclassOf<class ATileSequencesDisplayer> TileSequencesDisplayerBPClass;
	
	UPROPERTY()
	class AGameMapCreator* GameMapCreator;

	UPROPERTY()
	TArray<class ALevelSlot*> LevelSlotsList;
	
	UPROPERTY()
	int32 CurrentGridColNum;

	UPROPERTY()
	int32 CurrentGridRowNum;
	
	UPROPERTY()
	TArray<class ATileSequencesDisplayer*> TileSequencesDisplayersList;
};
