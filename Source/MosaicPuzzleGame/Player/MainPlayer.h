// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Common/GameStructures.h"
#include "MainPlayer.generated.h"

UCLASS()
class MOSAICPUZZLEGAME_API AMainPlayer : public APawn
{
	GENERATED_BODY()

	// Functions /////////////////////////////
public:
	AMainPlayer();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "CameraEvents")
	void OnSetCameraOrthoSize(float Size);
	
	UFUNCTION()
	void HandleMouseLineTrace(float DeltaTime);

	UFUNCTION()
	void HandleDrawingBrush(float DeltaTime);

	UFUNCTION()
	void UpdateCameraPositionAndSize(FLevelDatas InLevelDatas);
	
	UFUNCTION()
	void LeftMouseClick();

	UFUNCTION()
	void RightMouseClick();

	UFUNCTION()
	UMainGameInstance * GetGameInstance();

	// Variables /////////////////////////////
public:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = Camera)
	class UCameraComponent* MainPlayerCamera;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = Camera)
	class UArrowComponent* ArrowComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCamera")
	float CameraOrthoWidth = 1024;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCamera")
	float GameplayCameraOrthoWidthMargin = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCamera")
	float EditorCameraOrthoWidthMargin = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCamera")
	FVector GameplayCameraOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCamera")
	FVector EditorCameraOffset = FVector::ZeroVector;
			
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;
	
	UPROPERTY(BlueprintReadOnly, Category = "PlayerCamera")
	class AGameplayGameMode* GameMode;

	UPROPERTY()
	class AGameMapCreator* GameMapCreator;

	UPROPERTY()
	float OrthoSizeMultiplayer = 280;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerCamera")
	UCurveFloat* CameraOrthoSizeCurve;

	UPROPERTY()
	bool bIsHoveringSlot = false;

	UPROPERTY()
	class ALevelSlot* LevelSlotHovered;


};
