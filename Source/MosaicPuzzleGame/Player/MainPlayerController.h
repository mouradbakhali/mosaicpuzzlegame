// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	// Functions /////////////////////////////
	AMainPlayerController();
	virtual void BeginPlay() override;

	// Variables /////////////////////////////
	
	
};
