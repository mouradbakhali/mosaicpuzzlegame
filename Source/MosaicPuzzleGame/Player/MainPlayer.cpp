// Game created by Bakhali. All right reserved.

#include "MainPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/ArrowComponent.h"
#include "Camera/PlayerCameraManager.h"
#include "Kismet/GameplayStatics.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/PlayerController.h"
#include "GameManagers/MainGameInstance.h"
#include "Engine/World.h"
#include "Common/GameStructures.h"
#include "Gpe/GameMap/LevelSlot.h"
#include "Gpe/GameMap/LevelSlotFill.h"
#include "GameModes/GameplayGameMode.h"
#include "Classes/Curves/CurveFloat.h"

AMainPlayer::AMainPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ArrowComp = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Comp"));
	RootComponent = ArrowComp;

	// Main camera
	MainPlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Main Player Camera"));
	MainPlayerCamera->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform, FName(TEXT("MainCameraSocket")));

}
void AMainPlayer::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialize game instance
	GameInstance = GetGameInstance();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AMainPlayer > BeginPlay > GameInstance is nullptr"));
	}

	// Init game mode
	if (GameInstance->GameType == EGameType::GAMEPLAY)
	{
		GameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));

	}
}
void AMainPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	HandleMouseLineTrace(DeltaTime);
	HandleDrawingBrush(DeltaTime);
}
void AMainPlayer::HandleMouseLineTrace(float DeltaTime)
{
	// Cast ray to slots
	EDrawDebugTrace::Type DrawDebugType;
	DrawDebugType = EDrawDebugTrace::ForOneFrame;

	FVector MouseWorldLocation;
	FVector MouseWorldDirection;
	Cast<APlayerController>(GetController())->DeprojectMousePositionToWorld(MouseWorldLocation, MouseWorldDirection);


	FVector StartLoc = MouseWorldLocation + (FVector::UpVector * 20.0f);
	FVector EndLoc = StartLoc + (-FVector::UpVector * 700.0f);
	FHitResult Hit;

	// Actors to ignore
	TArray<AActor*> ActorToIgnore;
	ActorToIgnore.Add(this);

	// Do trace
	UKismetSystemLibrary::LineTraceSingle(this, StartLoc, EndLoc, UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_WorldDynamic), false, ActorToIgnore, DrawDebugType, Hit, true);

	//DrawDebugSphere(GetWorld(), MouseWorldLocation, 20.0f, 12, FColor::Cyan, false);

	// Check if we have an appropriate location
	if (Hit.bBlockingHit)
	{
		if (Hit.Actor != nullptr && Hit.Actor->ActorHasTag(FName(TEXT("LEVEL_SLOT"))))
		{
			bIsHoveringSlot = true;
			LevelSlotHovered = Cast<ALevelSlot>(Hit.Actor);
		}
		else
		{
			if (bIsHoveringSlot)
				bIsHoveringSlot = false;
			LevelSlotHovered = nullptr;
		}
	}
	else
	{
		if (bIsHoveringSlot)
			bIsHoveringSlot = false;
		LevelSlotHovered = nullptr;
	}
}
void AMainPlayer::HandleDrawingBrush(float DeltaTime)
{
	if (GameInstance->GameType == EGameType::GAMEPLAY && GameMode != nullptr)
	{
		FVector MouseWorldLocation;
		FVector MouseWorldDirection;
		Cast<APlayerController>(GetController())->DeprojectMousePositionToWorld(MouseWorldLocation, MouseWorldDirection);

		if (bIsHoveringSlot && LevelSlotHovered != nullptr)
		{
			if (GameMode->LevelDrawingMarker)
			{
				GameMode->UpdateDrawingBrush(LevelSlotHovered, MouseWorldLocation);
			}
		}
		else
		{
			GameMode->UpdateDrawingBrush(nullptr, MouseWorldLocation);
		}
	}
	else
	{
		FVector MouseWorldLocation;
		FVector MouseWorldDirection;
		Cast<APlayerController>(GetController())->DeprojectMousePositionToWorld(MouseWorldLocation, MouseWorldDirection);

		if (bIsHoveringSlot && LevelSlotHovered != nullptr)
		{
			if (GameInstance->GameMapCreator)
			{
				GameInstance->GameMapCreator->UpdateLevelSlotBrushLocation(LevelSlotHovered, MouseWorldLocation);
			}
		}
		else
		{
			GameInstance->GameMapCreator->UpdateLevelSlotBrushLocation(nullptr, MouseWorldLocation);
		}
	}
}
void AMainPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("LeftMouseClick", EInputEvent::IE_Pressed, this, &AMainPlayer::LeftMouseClick);
	InputComponent->BindAction("RightMouseClick", EInputEvent::IE_Pressed, this, &AMainPlayer::RightMouseClick);
}
void AMainPlayer::UpdateCameraPositionAndSize(FLevelDatas InLevelDatas)
{
	float SizeX = InLevelDatas.LevelGridColNum;
	float SizeY = InLevelDatas.LevelGridRowNum;
	float SlotSize = InLevelDatas.SlotSize;

	// Set location
	FVector Location = FVector(((SizeX * 100.0f) / 2.0f) - SlotSize / 2.0f, ((SizeY * 100.0f) / 2.0f) - SlotSize / 2.0f, 0.0f);
	if (GetGameInstance()->GameType == EGameType::LEVELEDITOR)
		Location += EditorCameraOffset;
	else
		Location += GameplayCameraOffset;

	SetActorLocation(Location);

	// Set roatation
	SetActorRotation(FRotator::ZeroRotator);

	// Set ortho size
	// Choose the greater dimention
	float DimentionSize = SizeX > SizeY ? SizeX : SizeY;
	if (GetGameInstance()->GameType == EGameType::LEVELEDITOR)
		CameraOrthoWidth = OrthoSizeMultiplayer * DimentionSize + EditorCameraOrthoWidthMargin;
	else
		CameraOrthoWidth = OrthoSizeMultiplayer * DimentionSize * CameraOrthoSizeCurve->GetFloatValue(DimentionSize) + GameplayCameraOrthoWidthMargin;
	
	OnSetCameraOrthoSize(CameraOrthoWidth);
}
void AMainPlayer::LeftMouseClick()
{
	EGameType GameType = GameInstance->GameType;
	switch (GameType)
	{
	case EGameType::GAMEPLAY:
	{
		break;
	}
	case EGameType::LEVELEDITOR:
	{
		if (bIsHoveringSlot)
		{
			if (LevelSlotHovered != nullptr)
			{
				// Check if slot is empty before filling it
				if (LevelSlotHovered->LevelSlotFill == nullptr)
				{
					LevelSlotHovered->FillSlot(GameInstance->SelectedDrawingColor);
				}
				//UE_LOG(LogTemp, Log, TEXT("Hovering a slot (index = %d) - [%d, %d]"), LevelSlotHovered->Index1D, LevelSlotHovered->IndexCol, LevelSlotHovered->IndexRow);
			}
		}
		break;
	}
	}
}
void AMainPlayer::RightMouseClick()
{
	EGameType GameType = GameInstance->GameType;
	switch (GameType)
	{
	case EGameType::GAMEPLAY:
	{
		UE_LOG(LogTemp, Log, TEXT("Gameplay right click"));
		break;
	}
	case EGameType::LEVELEDITOR:
	{
		if (bIsHoveringSlot)
		{
			// Check if slot is not empty before clearing it
			if (LevelSlotHovered != nullptr)
			{
				LevelSlotHovered->ClearSlot();				
				LevelSlotHovered->LevelSlotFill = nullptr;
			}
		}
		break;
	}
	}
}
UMainGameInstance* AMainPlayer::GetGameInstance()
{
	if (GameInstance)
		return GameInstance;
	else
		return GetWorld()->GetGameInstance<UMainGameInstance>();
}