// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Common/GameStructures.h"
#include "MainGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UMainGameInstance : public UGameInstance
{
	GENERATED_BODY()

	
public:
	// Functions ///////////////////////////////////////////
	UMainGameInstance(const FObjectInitializer& ObjectInitializer);
	
	UFUNCTION()
	void Initialize();
	
	UFUNCTION()
	void StartPlayLocalLevel(FString LevelName);

	UFUNCTION()
	void ChangeDrawingColor(FDrawingColor InNewColor);

	UFUNCTION()
	TArray<FString> LoadLocalLevelsName();

	UFUNCTION()
	FString GetLocalLevelsFolderPath();

	UFUNCTION()
	static FString ConvertColorIDEnumToString(EColorIDs ColorID);

	UFUNCTION()
	static EColorIDs ConvertColorIDStringToEnum(FString ColorIDString);

	// Variables ///////////////////////////////////////////
	UPROPERTY()
	int32 LevelMaxSizeX = 50;

	UPROPERTY()
	int32 LevelMinSizeX = 5;

	UPROPERTY()
	int32 LevelMaxSizeY = 50;
	
	UPROPERTY()
	int32 LevelMinSizeY = 5;

	// List of colors
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DrawingColors")
	TArray<FDrawingColor> DrawingColorsList;
	
	UPROPERTY(BlueprintReadOnly, Category = "DrawingColors")
	FDrawingColor SelectedDrawingColor;

	// Game type
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameType")
	EGameType GameType;



	UPROPERTY()
	class AGameMapCreator* GameMapCreator;

	// Selected Level datas
	UPROPERTY(BlueprintReadOnly, Category = "Levels")
	FLevelDatas LevelDatas;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "LocalLevelsEditing")
	bool bLocalLevelsEditingEnabled = true;
};

