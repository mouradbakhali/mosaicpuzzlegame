// Game created by Bakhali. All right reserved.

#include "MainGameInstance.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "Core/Public/Misc/Paths.h"
#include "Core/Public/HAL/FileManagerGeneric.h"
#include "Common/CommonFunctions.h"
#include "Kismet/GameplayStatics.h"

UMainGameInstance::UMainGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SelectedDrawingColor = FDrawingColor::FDrawingColor();
}

void UMainGameInstance::Initialize()
{
	if (DrawingColorsList.Num() == 0)
		return;

	SelectedDrawingColor = DrawingColorsList[0];
}

void UMainGameInstance::StartPlayLocalLevel(FString LevelName)
{	
	UCommonFunctions::LoadLevelLocally(LevelName, LevelDatas);
	UGameplayStatics::OpenLevel(this, "MP_GamePlay");
}

void UMainGameInstance::ChangeDrawingColor(FDrawingColor InNewColor)
{
	if (GameType == EGameType::LEVELEDITOR)
	{
		SelectedDrawingColor = InNewColor;
		GameMapCreator->ChangeDrawingColor(InNewColor);
	}
	else
	{

	}
}

TArray<FString> UMainGameInstance::LoadLocalLevelsName()
{
	TArray<FString> LocalLevelsList;
	LocalLevelsList.Empty();
	FString Directory = GetLocalLevelsFolderPath();
	if (FPaths::DirectoryExists(Directory))
	{
		FString Path = Directory + "*.json";
		FFileManagerGeneric::Get().FindFiles(LocalLevelsList, *Path, true, false);
		UE_LOG(LogTemp, Log, TEXT("UMainGameInstance > LoadLocalLevelsName > Level files names loaded successuflly."));
		return LocalLevelsList;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UMainGameInstance > LoadLocalLevelsName > Directory [%s] doesn't exist."), *Directory);
		return LocalLevelsList;
	}
	return LocalLevelsList;
}

FString UMainGameInstance::GetLocalLevelsFolderPath()
{
	return FPaths::ProjectDir() + "Content/Assets/Levels/";
}

FString UMainGameInstance::ConvertColorIDEnumToString(EColorIDs ColorID)
{
	return UCommonFunctions::EnumToString(TEXT("EColorIDs"), ColorID);
}

EColorIDs UMainGameInstance::ConvertColorIDStringToEnum(FString ColorIDString)
{
	return UCommonFunctions::StringToEnum<EColorIDs>(ColorIDString, TEXT("EColorIDs"));
}
