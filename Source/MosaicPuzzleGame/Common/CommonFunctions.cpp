// Game created by Bakhali. All right reserved.

#include "CommonFunctions.h"
#include "Runtime/JsonUtilities/Public/JsonUtilities.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "Runtime/JsonUtilities/Public/JsonObjectWrapper.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Core/Public/Misc/Paths.h"
#include "Core/Public/Misc/Paths.h"
#include "Core/Public/HAL/FileManagerGeneric.h"

void UCommonFunctions::LoadLevelLocally(FString LevelName, FLevelDatas& LevelDatas)
{
	// Create save data object
	TSharedPtr<FJsonObject> LoadedDataJSonObject;
	LoadedDataJSonObject = MakeShareable(new FJsonObject());

	// Generate path
	FString LevelPath = GetLocalLevelsFolderPath() + LevelName;

	// Load file to string
	FString LoadedJSonString = TEXT("");
	FFileHelper::LoadFileToString(LoadedJSonString, *LevelPath);

	// Deseralize
	TSharedRef< TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(*LoadedJSonString);
	FJsonSerializer::Deserialize(JsonReader, LoadedDataJSonObject);

	// Convert level's json to struct
	FJsonObjectConverter::JsonObjectStringToUStruct<FLevelDatas>(LoadedJSonString, &LevelDatas, 0, 0);
}

FString UCommonFunctions::GetLocalLevelsFolderPath()
{
	return FPaths::ProjectDir() + "Content/Assets/Levels/";
}
