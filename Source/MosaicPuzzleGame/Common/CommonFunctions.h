// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Runtime/CoreUObject/Public/UObject/Class.h"
#include "Common/GameStructures.h"
#include "CommonFunctions.generated.h"

/**
 * This class holds all datas structures that is used in this game
 */

UCLASS()
class MOSAICPUZZLEGAME_API UCommonFunctions : public UObject
{
	GENERATED_BODY()
	// Functions //////////////////////////////////
public:
	UFUNCTION()
	static void LoadLevelLocally(FString LevelName, FLevelDatas& LevelDatas);

	UFUNCTION()
	static FString GetLocalLevelsFolderPath();

	template<typename T>
	static FString EnumToString(const FString& enumName, const T value)
	{
		UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName);
		return *(pEnum ? pEnum->GetNameStringByIndex(static_cast<uint8>(value)) : "null");
	}

	template<typename T>
	static T StringToEnum(const FString& enumName, FString EnumString)
	{

		UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName);
		return static_cast<T>(pEnum->GetValueByNameString(*EnumString, EGetByNameFlags::ErrorIfNotFound));
		//return *(pEnum ? pEnum->GetNameStringByIndex(static_cast<uint8>(value)) : "null");
	}
};

