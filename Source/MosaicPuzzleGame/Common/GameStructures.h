// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameStructures.generated.h"

/**
 * This class holds all datas structures that is used in this game
 */

// Enums
UENUM(BlueprintType)
enum class EColorIDs : uint8
{
	COLOR_1				UMETA(DisplayName = "COLOR_1"),
	COLOR_2				UMETA(DisplayName = "COLOR_2"),
	COLOR_3				UMETA(DisplayName = "COLOR_3"),
	COLOR_4				UMETA(DisplayName = "COLOR_4"),
	COLOR_5				UMETA(DisplayName = "COLOR_5"),
	COLOR_6				UMETA(DisplayName = "COLOR_6"),
	COLOR_NONE			UMETA(DisplayName = "COLOR_NONE")
};

UENUM(BlueprintType)
enum class EGameBrushType : uint8
{
	DRAW_BRUSH				UMETA(DisplayName = "DRAW_BRUSH"),
	MARKEMPTY_BRUSH			UMETA(DisplayName = "MARKEMPTY_BRUSH")
};

UENUM(BlueprintType)
enum class EGameType : uint8
{
	GAMEPLAY				UMETA(DisplayName = "GAMEPLAY"),
	LEVELEDITOR				UMETA(DisplayName = "LEVELEDITOR"),
};

UENUM(BlueprintType)
enum class ELevelEditType : uint8
{
	LOCAL				UMETA(DisplayName = "LOCAL"),
	ONLINE				UMETA(DisplayName = "ONLINE"),
};

 // Structures
USTRUCT(BlueprintType)
struct FDrawingColor
{
	GENERATED_USTRUCT_BODY()

	// Variables /////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DrawingColor)
	FString ColorID = TEXT("");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DrawingColor)
	FLinearColor Color = FLinearColor::Black;

	FDrawingColor() {
		ColorID = TEXT("");
		Color = FLinearColor::Black;
	}
	FDrawingColor(FString ColorID, FLinearColor Color)
	{
		ColorID = ColorID;
		Color = Color;
	}
};

USTRUCT(BlueprintType)
struct FLevelDatas
{
	GENERATED_USTRUCT_BODY()

	// Variables /////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelDatas)
	int32 LevelID = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelDatas)
	int32 LevelGridColNum = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelDatas)
	int32 LevelGridRowNum = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelDatas)
	float SlotSize = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelDatas)
	TArray<FDrawingColor> ColorsList;

	FLevelDatas()
	{
		ColorsList = {};
	}
	FLevelDatas(int32 ColNum, int32 RowNum)
	{
		ColorsList = {};
		LevelGridColNum = ColNum;
		LevelGridRowNum = RowNum;
	}
};

UCLASS()
class MOSAICPUZZLEGAME_API UGameStructures : public UObject
{
	GENERATED_BODY()
};

