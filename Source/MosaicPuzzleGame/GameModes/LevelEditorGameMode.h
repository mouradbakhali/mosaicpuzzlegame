// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LevelEditorGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API ALevelEditorGameMode : public AGameModeBase
{
	GENERATED_BODY()

	// Functions //////////////////////////////////////////
public:
	ALevelEditorGameMode();
	virtual void PreInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	// Variables /////////////////////////////////////////
public:
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;
};
