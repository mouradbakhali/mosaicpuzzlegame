// Game created by Bakhali. All right reserved.

#include "LevelEditorGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/PlayerController.h"
#include "GameManagers/MainGameInstance.h"
#include "Engine/World.h"
#include "Common/GameStructures.h"

ALevelEditorGameMode::ALevelEditorGameMode() : Super()
{

}

void ALevelEditorGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();

}


void ALevelEditorGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ALevelEditorGameMode > BeginPlay > GameInstance is nullptr"));
	}

	// Set game type to editor
	GameInstance->GameType = EGameType::LEVELEDITOR;

}

void ALevelEditorGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}