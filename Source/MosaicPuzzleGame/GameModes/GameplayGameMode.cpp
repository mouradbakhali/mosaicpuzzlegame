// Game created by Bakhali. All right reserved.

#include "GameplayGameMode.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "GameManagers/MainGameInstance.h"
#include "GPE/GameMap/LevelDrawer.h"
#include "UI/GamePlay/MainGamePlayUI.h"
#include "Player/MainPlayer.h"
#include "GPE/GameMap/LevelSlot.h"
#include "GPE/GameMap/LevelDrawingMarker.h"

AGameplayGameMode::AGameplayGameMode()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AGameplayGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > BeginPlay > GameInstance is nullptr"));
	}
	
	// Change game play type
	GameInstance->GameType = EGameType::GAMEPLAY;
	
	// Init main player
	MainPlayer = Cast<AMainPlayer>(UGameplayStatics::GetPlayerPawn(this, 0));

	// Initialize level datas
	CurrentLevelDatas = FLevelDatas::FLevelDatas(GameInstance->LevelDatas.LevelGridColNum, GameInstance->LevelDatas.LevelGridRowNum);

	// Initialize level drawer
	LevelDrawer = GetLevelDrawer();

	// Draw level for be played
	DrawLevelForPlay();

	// Create main gameplay ui
	if (MainGameplayUIClass != nullptr)
	{
		//UGameplayStatics::GetPlayerController(this, 0)->Create;
		MainGameplayUIInstance = CreateWidget<UMainGamePlayUI>(GetWorld(), MainGameplayUIClass);
		MainGameplayUIInstance->AddToViewport();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > BeginPlay > No MainGameplayUIClass is nullptr."));
	}

	// Create level drawing marker
	if (LevelDrawingMarkerBPClass)
	{
		LevelDrawingMarker = GetWorld()->SpawnActor<ALevelDrawingMarker>(LevelDrawingMarkerBPClass);
		if (LevelDrawingMarker == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > BeginPlay > LevelDrawingMarker is nullptr."));
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > BeginPlay > LevelDrawingMarkerBPClass is nullptr."));
	}
}

void AGameplayGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGameplayGameMode::DrawLevelForPlay()
{
	if (LevelDrawer != nullptr)
	{
		LevelDrawer->DrawLevelSlotsForPlay(GameInstance->LevelDatas, GameInstance->SelectedDrawingColor.ColorID);
		MainPlayer->UpdateCameraPositionAndSize(CurrentLevelDatas);
	}
}

void AGameplayGameMode::UpdateDrawingBrush(ALevelSlot * LevelSlot, FVector MouseWorldLocation)
{
	FVector BrushLoc;
	if (LevelSlot)
	{
		BrushLoc = LevelSlot->GetActorLocation();
		LevelDrawingMarker->UpdateLevelDrawingMarker(BrushLoc);
	}
	else
	{
		BrushLoc = MouseWorldLocation;

	}
}

void AGameplayGameMode::UpdateTilesSequencesDisplayer_(FString ColorID)
{
	if (LevelDrawer != nullptr)
		LevelDrawer->UpdateTilesSequencesDisplayer(ColorID);
}

ALevelDrawer* AGameplayGameMode::GetLevelDrawer()
{
	if (LevelDrawer != nullptr)
	{
		return LevelDrawer;
	}
	else
	{
		TArray<AActor*> LevelDrawerActors;
		UGameplayStatics::GetAllActorsOfClass(this, ALevelDrawer::StaticClass(), LevelDrawerActors);
		if (LevelDrawerActors.Num() > 0)
		{
			return Cast<ALevelDrawer>(LevelDrawerActors[0]);
			if (LevelDrawerActors.Num() > 1)
			{
				UE_LOG(LogTemp, Warning, TEXT("AGameplayGameMode > GetLevelDrawer > More than one LevelDrawer in the level !"));
			}
		}
		else
		{	
			UE_LOG(LogTemp, Error, TEXT("AGameplayGameMode > GetLevelDrawer > No LevelDrawer in the level !"));
			return nullptr;
		}
	}
}