// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Common/GameStructures.h"
#include "GameplayGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API AGameplayGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
	// Functions //////////////////////////////////
public:
	AGameplayGameMode();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void DrawLevelForPlay();

	UFUNCTION()
	void UpdateDrawingBrush(class ALevelSlot* LevelSlot, FVector MouseWorldLocation);

	UFUNCTION()
	void UpdateTilesSequencesDisplayer_(FString ColorID);

	class ALevelDrawer * GetLevelDrawer();

	// Variables //////////////////////////////////
public:
	UPROPERTY()
	class UMainGameInstance* GameInstance;
	
	UPROPERTY()
	class ALevelDrawer* LevelDrawer;

	UPROPERTY()
	class AMainPlayer* MainPlayer;

	UPROPERTY()
	FLevelDatas CurrentLevelDatas;

	// Gameplay ui
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gameplay UI")
	TSubclassOf<class UMainGamePlayUI> MainGameplayUIClass;

	UPROPERTY()
	class UMainGamePlayUI* MainGameplayUIInstance;

	// Brush type
	UPROPERTY()
	EGameBrushType GameBrushType;

	//Drawing marker
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Level Drawing")
	TSubclassOf<class ALevelDrawingMarker> LevelDrawingMarkerBPClass;

	UPROPERTY()
	ALevelDrawingMarker* LevelDrawingMarker;

};
