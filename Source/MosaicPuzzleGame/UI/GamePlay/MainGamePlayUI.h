// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainGamePlayUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UMainGamePlayUI : public UUserWidget
{
	GENERATED_BODY()
	// Functions //////////////////////////////////
public:
	UMainGamePlayUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void InitializeWidget(class AGameplayGameMode* GameMode);

private:
	// Set brush type
	UFUNCTION()
	void SetBrushTypeToFill();

	UFUNCTION()
	void SetBrushTypeToMarkEmpty();

	// Change brush functions
	UFUNCTION()
	void SetBrushColor_1();
	UFUNCTION()
	void SetBrushColor_2();
	UFUNCTION()
	void SetBrushColor_3();
	UFUNCTION()
	void SetBrushColor_4();
	UFUNCTION()
	void SetBrushColor_5();
	UFUNCTION()
	void SetBrushColor_6();

	UFUNCTION()
	void SetSelectedDrawingColor(int32 ColorIndex);

	UFUNCTION()
	void SetButtonVisible(int32 ButtonIndex);

	// Variables //////////////////////////////////
private:
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;

	// Game mode
	UPROPERTY()
	class AGameplayGameMode* GameMode;
public:
	// Hint
	UPROPERTY(BlueprintReadWrite, Category = "Hint")
	FText HintNumber = FText::FromString(TEXT("2"));
	
	UPROPERTY(BlueprintReadWrite, Category = "Hint")
	float HintProgress;

	// Mistakes
	UPROPERTY(BlueprintReadWrite, Category = "Mistakes")
	FText MistakesNumber = FText::FromString(TEXT("0"));;

	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* FillButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* MarkEmptyButton;
	
	// Colors buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_1_Button;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_2_Button;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_3_Button;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_4_Button;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_5_Button;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* Color_6_Button;
};
