// Game created by Bakhali. All right reserved.

#include "MainGamePlayUI.h"
#include "GameManagers/MainGameInstance.h"
#include "GameModes/GameplayGameMode.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "UI/LevelSelection/LevelSelectionButtonUI.h"
#include "Common/CommonFunctions.h"
#include "UMG/Public/Components/PanelSlot.h"
#include "UMG/Public/Components/UniformGridSlot.h"
#include "UMG/Public/Components/CanvasPanelSlot.h"
#include "GameFramework/PlayerController.h"
#include "GPE/GameMap/LevelDrawer.h"

UMainGamePlayUI::UMainGamePlayUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UMainGamePlayUI::NativeConstruct()
{
	Super::NativeConstruct();


	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UMainGamePlayUI > NativeConstruct > GameInstance is nullptr"));
	}

	// Init buttons
	if (FillButton != nullptr) { FillButton->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushTypeToFill); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > FillButton is nullptr"); }

	if (MarkEmptyButton != nullptr) { MarkEmptyButton->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushTypeToMarkEmpty); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > MarkEmptyButton is nullptr"); }

	//Color_1_Button
	if (Color_1_Button != nullptr) { Color_1_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_1); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_1_Button is nullptr"); }
	if (Color_2_Button != nullptr) { Color_2_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_2); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_2_Button is nullptr"); }
	if (Color_3_Button != nullptr) { Color_3_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_3); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_3_Button is nullptr"); }
	if (Color_4_Button != nullptr) { Color_4_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_4); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_4_Button is nullptr"); }
	if (Color_5_Button != nullptr) { Color_5_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_5); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_5_Button is nullptr"); }
	if (Color_6_Button != nullptr) { Color_6_Button->OnClicked.AddDynamic(this, &UMainGamePlayUI::SetBrushColor_6); }
	else { UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > Color_6_Button is nullptr"); }
	
	
	// Set buttons colors
	if (GameInstance->DrawingColorsList.Num() == 6)
	{
		Color_1_Button->SetBackgroundColor(GameInstance->DrawingColorsList[0].Color);
		Color_2_Button->SetBackgroundColor(GameInstance->DrawingColorsList[1].Color);
		Color_3_Button->SetBackgroundColor(GameInstance->DrawingColorsList[2].Color);
		Color_4_Button->SetBackgroundColor(GameInstance->DrawingColorsList[3].Color);
		Color_5_Button->SetBackgroundColor(GameInstance->DrawingColorsList[4].Color);
		Color_6_Button->SetBackgroundColor(GameInstance->DrawingColorsList[5].Color);
	}
	else
	{
		UE_LOG(LogTemp, Error, L"UMainGamePlayUI > NativeConstruct > GameInstance->DrawingColorsList.Num() is not equal to 6");
	}
	
	// Set first color as default one
	//SetSelectedDrawingColor(0);
}

void UMainGamePlayUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void UMainGamePlayUI::InitializeWidget(AGameplayGameMode* GameMode)
{
}

void UMainGamePlayUI::SetBrushTypeToFill()
{
}

void UMainGamePlayUI::SetBrushTypeToMarkEmpty()
{
}

void UMainGamePlayUI::SetBrushColor_1()
{
	SetSelectedDrawingColor(0);
}
void UMainGamePlayUI::SetBrushColor_2()
{
	SetSelectedDrawingColor(1);
}
void UMainGamePlayUI::SetBrushColor_3()
{
	SetSelectedDrawingColor(2);
}
void UMainGamePlayUI::SetBrushColor_4()
{
	SetSelectedDrawingColor(3);
}
void UMainGamePlayUI::SetBrushColor_5()
{
	SetSelectedDrawingColor(4);
}
void UMainGamePlayUI::SetBrushColor_6()
{
	SetSelectedDrawingColor(5);
}

void UMainGamePlayUI::SetSelectedDrawingColor(int32 ColorIndex)
{
	if (!GameInstance) return;
	GameInstance->SelectedDrawingColor = GameInstance->DrawingColorsList[ColorIndex];
	//GameMode->UpdateTilesSequencesDisplayer_(GameInstance->SelectedDrawingColor.ColorID);
	//GameMode->LevelDrawer->UpdateTilesSequencesDisplayer(GameInstance->SelectedDrawingColor.ColorID);

	TArray<AActor*> LevelDrawerActors;
	ALevelDrawer* LvlDrawer;
	UGameplayStatics::GetAllActorsOfClass(this, ALevelDrawer::StaticClass(), LevelDrawerActors);
	if (LevelDrawerActors.Num() > 0)
	{
		LvlDrawer = Cast<ALevelDrawer>(LevelDrawerActors[0]);
		LvlDrawer->UpdateTilesSequencesDisplayer(GameInstance->SelectedDrawingColor.ColorID);
	}

	SetButtonVisible(ColorIndex);
}

void UMainGamePlayUI::SetButtonVisible(int32 ButtonIndex)
{
	TArray<UButton*> ButonsList;// = { Color_1_Button , Color_2_Button , Color_3_Button , Color_4_Button , Color_5_Button , Color_6_Button };
	ButonsList.Add(Color_1_Button);
	ButonsList.Add(Color_2_Button);
	ButonsList.Add(Color_3_Button);
	ButonsList.Add(Color_4_Button);
	ButonsList.Add(Color_5_Button);
	ButonsList.Add(Color_6_Button);

	Color_1_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	Color_2_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	Color_3_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	Color_4_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	Color_5_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	Color_6_Button->GetChildAt(0)->SetVisibility(ESlateVisibility::Hidden);
	ButonsList[ButtonIndex]->GetChildAt(0)->SetVisibility(ESlateVisibility::HitTestInvisible);
	
	UE_LOG(LogTemp, Log, L"UMainGamePlayUI > SetButtonVisible > Child name = %s", *ButonsList[ButtonIndex]->GetChildAt(0)->GetName());
}
