// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MouseCursorUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UMouseCursorUI : public UUserWidget
{
	GENERATED_BODY()		
	// Functions /////////////////////////////
public:
	UMouseCursorUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
	// Variables ///////////////////////////////////
public:
	UPROPERTY(BlueprintReadOnly, Category = "Mouse Loc")
	float MouseXPos = 0.0f;
	UPROPERTY(BlueprintReadOnly, Category = "Mouse Loc")	
	float MouseYPos = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Mouse Loc")
		int32 SizeX = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Mouse Loc")
		int32 SizeY = 0;

	UPROPERTY(BlueprintReadOnly, Category = "Mouse Loc")
		FVector2D MousePos;
};
