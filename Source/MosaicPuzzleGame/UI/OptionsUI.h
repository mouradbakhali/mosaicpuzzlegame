// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OptionsUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UOptionsUI : public UUserWidget
{
	GENERATED_BODY()
	
	UOptionsUI(const FObjectInitializer& ObjectInitializer);
	
	
};
