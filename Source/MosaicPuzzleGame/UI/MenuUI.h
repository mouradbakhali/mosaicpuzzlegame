// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "MenuUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UMenuUI : public UUserWidget
{
	GENERATED_BODY()

	// Functions //////////////////////////////////
public:
	UMenuUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
private:
	UFUNCTION()
		void CreateLevelSelectionScreen();
	UFUNCTION()
		void CreateUserLevelsScreen();
	UFUNCTION()
		void OpenLevelEditorMap();
	// Variables //////////////////////////////////
public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* PlayButton;
		
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* PlayUserLevelsButton;
	
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* CreateLevelButton;
		
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* OptionsButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* ExitButton;

	// UMG classes
	/////////////// Level selection
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Classes")
		TSubclassOf<class ULevelSelectionUI> LevelSelectionClass;

	UPROPERTY()
		class ULevelSelectionUI* LevelSelectionInstance;

	/////////////// User levels
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Classes")
		TSubclassOf<class UUserLevelsUI> UserLevelsClass;

	UPROPERTY()
		class UUserLevelsUI* UserLevelsInstance;

};
