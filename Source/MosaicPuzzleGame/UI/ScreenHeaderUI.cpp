// Game created by Bakhali. All right reserved.

#include "ScreenHeaderUI.h"
#include "Components/Button.h"
#include "Components/Widget.h"

UScreenHeaderUI::UScreenHeaderUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UScreenHeaderUI::NativeConstruct()
{
	Super::NativeConstruct();

	if(BackButton != nullptr)
		BackButton->OnClicked.AddDynamic(this, &UScreenHeaderUI::GoBackToLastScreen);
	else
		UE_LOG(LogTemp, Error, L"UScreenHeaderUI > NativeConstruct > BackButton is nullptr");
	
	ParentUI = Cast<UUserWidget>(Cast<UUserWidget>(GetParent())->GetRootWidget());
}

void UScreenHeaderUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void UScreenHeaderUI::GoBackToLastScreen()
{
	ParentUI->RemoveFromParent();
}


