// Game created by Bakhali. All right reserved.

#include "MouseCursorUI.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/UMG/Public/Blueprint/WidgetLayoutLibrary.h"

UMouseCursorUI::UMouseCursorUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UMouseCursorUI::NativeConstruct()
{
	Super::NativeConstruct();

	UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameAndUI());
}

void UMouseCursorUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	MousePos = UWidgetLayoutLibrary::GetMousePositionOnViewport(GetWorld());
}

