// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScreenHeaderUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UScreenHeaderUI : public UUserWidget
{
	GENERATED_BODY()
	// Functions //////////////////////////////////
public:
	UScreenHeaderUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
private:
	UFUNCTION()
		void GoBackToLastScreen();

	// Variables //////////////////////////////////
public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* BackButton;

	UPROPERTY(BlueprintReadWrite, Category = "ParrentUI")
		class UUserWidget* ParentUI;
};
