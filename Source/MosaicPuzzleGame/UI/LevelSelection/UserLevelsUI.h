// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UserLevelsUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UUserLevelsUI : public UUserWidget
{
	GENERATED_BODY()

	// Functions //////////////////////////////////
public:
	UUserLevelsUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:
	UFUNCTION()
		void GoBackToLastScreen();

	// Variables //////////////////////////////////
public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
		class UButton* BackButton;
};
