// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelSelectionButtonUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API ULevelSelectionButtonUI : public UUserWidget
{
	GENERATED_BODY()
	// Functions //////////////////////////////////
public:
	ULevelSelectionButtonUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void InitializeWidget(class ULevelSelectionUI * InLevelSelectionUI);

	UFUNCTION()
	void SelectLevelForPlay();

	UFUNCTION()
	void SetParentCanvasPanelSize(FVector2D Size);

	UFUNCTION(BlueprintPure)
	FText GetSelectedLevelNumer();

	UFUNCTION()
	void Show(bool bIsForward = true);

	UFUNCTION()
	void Hide(bool bIsForward = true);

	UFUNCTION()
	void HideWidget();

	// Variables //////////////////////////////////
private:
	// Level editor ui
	UPROPERTY()
	class ULevelSelectionUI* LevelSelectionUI;

public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* LevelSelectButton;
	
	// Parent canvas panel
	UPROPERTY(BlueprintReadWrite, Category = "Panels")
	class UCanvasPanel* ParentCanvasPanel;
	
	// Animations
	UPROPERTY(BlueprintReadWrite, Category = "Animations")
	class UWidgetAnimation* EnterAnimation;

	UPROPERTY(BlueprintReadWrite, Category = "Animations")
	class UWidgetAnimation* ExitAnimation;

	// Text
	UPROPERTY(BlueprintReadWrite, Category = "Textes")
	FText LevelName;

	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;


};
