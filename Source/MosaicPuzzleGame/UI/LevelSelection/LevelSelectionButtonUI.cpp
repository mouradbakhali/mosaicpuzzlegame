// Game created by Bakhali. All right reserved.

#include "LevelSelectionButtonUI.h"
#include "GameManagers/MainGameInstance.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "UI/LevelSelection/LevelSelectionUI.h"
#include "UMG/Public/Components/CanvasPanel.h"
#include "UMG/Public/Components/CanvasPanelSlot.h"
#include "UMG/Public/Animation/WidgetAnimation.h"

ULevelSelectionButtonUI::ULevelSelectionButtonUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	LevelName = FText::FromString(TEXT(""));
}

void ULevelSelectionButtonUI::NativeConstruct()
{
	Super::NativeConstruct();

	UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameAndUI());
	//UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameOnly());

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelSelectionButtonUI > NativeConstruct > GameInstance is nullptr"));
	}


	// Initialize buttons
	if (LevelSelectButton != nullptr) { LevelSelectButton->OnClicked.AddDynamic(this, &ULevelSelectionButtonUI::SelectLevelForPlay); }
	else { UE_LOG(LogTemp, Error, L"ULevelSelectionButtonUI > NativeConstruct > LevelSelectButton is nullptr"); }
}

void ULevelSelectionButtonUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void ULevelSelectionButtonUI::InitializeWidget(ULevelSelectionUI * InLevelSelectionUI)
{
	LevelSelectionUI = InLevelSelectionUI;
}

void ULevelSelectionButtonUI::SelectLevelForPlay()
{
	//FString LevelName = LevelName;
	UE_LOG(LogTemp, Log, TEXT("ULevelSelectionButtonUI > SelectLevelForPlay > Level: %s"), *LevelName.ToString());
	GameInstance->StartPlayLocalLevel(LevelName.ToString());

}

void ULevelSelectionButtonUI::SetParentCanvasPanelSize(FVector2D Size)
{
	UCanvasPanelSlot* PanelSlot =  Cast<UCanvasPanelSlot>(ParentCanvasPanel->Slot);
	if (PanelSlot != nullptr)
	{
		PanelSlot->SetSize(Size);
	}
}

FText ULevelSelectionButtonUI::GetSelectedLevelNumer()
{
	FString LevelNumber;
	LevelName.ToString().Split(".", &LevelNumber, nullptr);
	LevelNumber.Split("_", nullptr, &LevelNumber);
	return FText::FromString(LevelNumber);
}

void ULevelSelectionButtonUI::Show(bool bIsForward)
{
	SetVisibility(ESlateVisibility::Visible);
	if (EnterAnimation)
	{
		PlayAnimation(EnterAnimation, 0.0f, 1, EUMGSequencePlayMode::Forward);
	}
}

void ULevelSelectionButtonUI::Hide(bool bIsForward)
{

	if (ExitAnimation)
	{		
		float EndTime = EnterAnimation->GetEndTime();
		PlayAnimation(ExitAnimation, 0.0f, 1, EUMGSequencePlayMode::Forward);
		ExitAnimation->OnAnimationFinished.AddDynamic(this, &ULevelSelectionButtonUI::HideWidget);
	}
}

void ULevelSelectionButtonUI::HideWidget()
{
	SetVisibility(ESlateVisibility::Hidden);
	ExitAnimation->OnAnimationFinished.RemoveDynamic(this, &ULevelSelectionButtonUI::HideWidget);
}