// Game created by Bakhali. All right reserved.

#include "UserLevelsUI.h"
#include "Components/Button.h"

UUserLevelsUI::UUserLevelsUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UUserLevelsUI::NativeConstruct()
{
	Super::NativeConstruct();

	if (BackButton != nullptr)
		BackButton->OnClicked.AddDynamic(this, &UUserLevelsUI::GoBackToLastScreen);
	else
		UE_LOG(LogTemp, Error, L"UUserLevelsUI > NativeConstruct > BackButton is nullptr");
}

void UUserLevelsUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void UUserLevelsUI::GoBackToLastScreen()
{
	RemoveFromParent();
}



