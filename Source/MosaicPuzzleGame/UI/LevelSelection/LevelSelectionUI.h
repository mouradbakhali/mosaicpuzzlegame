// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelSelectionUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API ULevelSelectionUI : public UUserWidget
{
	GENERATED_BODY()

	// Functions //////////////////////////////////
public:
	ULevelSelectionUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:
	UFUNCTION()
	void GoBackToLastScreen();

	UFUNCTION()
	void AddLevelSelectionButton(FString LevelName, int32 CellRow, int32 CellCol, int32 Index);

	UFUNCTION()
	void DisplayNextLevel();

	UFUNCTION()
	void DisplayPrevLevel();

	// Variables //////////////////////////////////
public:
	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;

	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* BackButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* NextLevelButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* PrevLevelButton;

	// Level selection buttons
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Level Select Button")
	TSubclassOf<class ULevelSelectionButtonUI> LevelSelectButtonClass;

	// Widget parent
	UPROPERTY(BlueprintReadWrite, Category = "UMG Level Select Button")
	class UPanelWidget* LevelSelectButtonParentInstance;

	UPROPERTY()
	TArray<class ULevelSelectionButtonUI*> LevelSelectButtonsInstancesList;

	UPROPERTY()
	int32 CurrentLevelIndex;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Level Select Button")
	int32 LevelSelectionGridRow = 5;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Level Select Button")
	int32 LevelSelectionGridCol = 5;
};
