// Game created by Bakhali. All right reserved.

#include "LevelSelectionUI.h"
#include "GameManagers/MainGameInstance.h"
#include "Components/Button.h"
#include "UI/LevelSelection/LevelSelectionButtonUI.h"
#include "Common/CommonFunctions.h"
#include "UMG/Public/Components/PanelSlot.h"
#include "UMG/Public/Components/UniformGridSlot.h"
#include "UMG/Public/Components/CanvasPanelSlot.h"

ULevelSelectionUI::ULevelSelectionUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void ULevelSelectionUI::NativeConstruct()
{
	Super::NativeConstruct();

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelSelectionUI > NativeConstruct > GameInstance is nullptr"));
	}

	// Init buttons
	if (BackButton != nullptr) { BackButton->OnClicked.AddDynamic(this, &ULevelSelectionUI::GoBackToLastScreen); }
	else { UE_LOG(LogTemp, Error, L"ULevelSelectionUI > NativeConstruct > BackButton is nullptr"); }

	if (NextLevelButton != nullptr) { NextLevelButton->OnClicked.AddDynamic(this, &ULevelSelectionUI::DisplayNextLevel); }
	else { UE_LOG(LogTemp, Error, L"ULevelSelectionUI > NativeConstruct > NextLevelButton is nullptr"); }

	if (PrevLevelButton != nullptr) { PrevLevelButton->OnClicked.AddDynamic(this, &ULevelSelectionUI::DisplayPrevLevel); }
	else { UE_LOG(LogTemp, Error, L"ULevelSelectionUI > NativeConstruct > PrevLevelButton is nullptr"); }
			
	// Create levels selection buttons
	TArray<FString> LevelNamesList = GameInstance->LoadLocalLevelsName();
	if (LevelNamesList.Num() > 0)
	{
		for (int i = 0; i < LevelNamesList.Num(); i++)
		{
			int32 CellRow = i / LevelSelectionGridRow;
			int32 CellCol = i % LevelSelectionGridCol;
			AddLevelSelectionButton(LevelNamesList[i], CellRow, CellCol, i);
		}
	}

	// Init levels index
	CurrentLevelIndex = 0;
}

void ULevelSelectionUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void ULevelSelectionUI::GoBackToLastScreen()
{
	RemoveFromParent();
}

void ULevelSelectionUI::AddLevelSelectionButton(FString LevelName, int32 CellRow, int32 CellCol, int32 Index)
{
	if (LevelSelectButtonParentInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelSelectionUI > AddLevelSelectionButton > LevelSelectButtonParentInstance is nullptr."));
		return;
	}
	ULevelSelectionButtonUI* LevelSelectionButton = CreateWidget<ULevelSelectionButtonUI>(this, LevelSelectButtonClass);
	LevelSelectButtonParentInstance->AddChild(Cast<UWidget>(LevelSelectionButton));
	UCanvasPanelSlot* PanelSlot = Cast<UCanvasPanelSlot>(LevelSelectButtonParentInstance->Slot);
	
	if (PanelSlot != nullptr)
	{
		LevelSelectionButton->SetParentCanvasPanelSize(PanelSlot->GetSize());
	}

	if (Index > 0)
	{
		LevelSelectionButton->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		LevelSelectionButton->Show();
	}

	LevelSelectionButton->InitializeWidget(this);
	LevelSelectionButton->LevelName = FText::FromString(LevelName);
	LevelSelectButtonsInstancesList.Add(LevelSelectionButton);
}

void ULevelSelectionUI::DisplayNextLevel()
{
	if (CurrentLevelIndex >= LevelSelectButtonsInstancesList.Num() - 1)
	{
		return;
	}

	// Hide current level button
	LevelSelectButtonsInstancesList[CurrentLevelIndex]->SetVisibility(ESlateVisibility::Hidden);
	// Shoe next level
	CurrentLevelIndex++;
	LevelSelectButtonsInstancesList[CurrentLevelIndex]->Show(true);
}

void ULevelSelectionUI::DisplayPrevLevel()
{
	if (CurrentLevelIndex <= 0)
	{
		return;
	}

	// Hide current level button
	LevelSelectButtonsInstancesList[CurrentLevelIndex]->Hide(false);
	// Shoe next level
	CurrentLevelIndex--;
	LevelSelectButtonsInstancesList[CurrentLevelIndex]->SetVisibility(ESlateVisibility::Visible);
}

