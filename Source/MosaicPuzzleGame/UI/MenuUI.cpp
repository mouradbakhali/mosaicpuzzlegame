// Game created by Bakhali. All right reserved.

#include "MenuUI.h"
#include "UI/LevelSelection/LevelSelectionUI.h"
#include "UI/LevelSelection/UserLevelsUI.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"


UMenuUI::UMenuUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UMenuUI::NativeConstruct()
{
	Super::NativeConstruct();
	
	PlayButton->OnClicked.AddDynamic(this,				&UMenuUI::CreateLevelSelectionScreen);
	PlayUserLevelsButton->OnClicked.AddDynamic(this,	&UMenuUI::CreateUserLevelsScreen);
	CreateLevelButton->OnClicked.AddDynamic(this,		&UMenuUI::OpenLevelEditorMap);
	OptionsButton->OnClicked.AddDynamic(this,			&UMenuUI::CreateUserLevelsScreen);
	ExitButton->OnClicked.AddDynamic(this,				&UMenuUI::CreateUserLevelsScreen);

	UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameAndUI());
}

void UMenuUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void UMenuUI::CreateLevelSelectionScreen()
{
	if (LevelSelectionClass)
	{
		LevelSelectionInstance = CreateWidget<ULevelSelectionUI>(this, LevelSelectionClass);
		LevelSelectionInstance->AddToViewport(10);
	}
	else
	{
		UE_LOG(LogTemp, Warning, L"UMenuUI > CreateLevelSelectionScreen > LevelSelectionClass blueprint class is nullptr");
	}
}

void UMenuUI::CreateUserLevelsScreen()
{
	if (UserLevelsClass)
	{
		UserLevelsInstance = CreateWidget<UUserLevelsUI>(this, UserLevelsClass);
		UserLevelsInstance->AddToViewport(10);
	}
	else
	{
		UE_LOG(LogTemp, Warning, L"UMenuUI > CreateUserLevelsScreen > UserLevelsClass blueprint class is nullptr");
	}
}

void UMenuUI::OpenLevelEditorMap()
{
	UGameplayStatics::OpenLevel(this, "MP_LevelEditor");
}
