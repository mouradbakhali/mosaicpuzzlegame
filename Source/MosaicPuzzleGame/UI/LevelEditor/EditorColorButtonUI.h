// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Common/GameStructures.h"
#include "EditorColorButtonUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API UEditorColorButtonUI : public UUserWidget
{
	GENERATED_BODY()
		// Functions /////////////////////////////
public:
	UEditorColorButtonUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void ApplyColorToButton(FDrawingColor DrawingColorValue);
	
	UFUNCTION()
	void ChangeDrawingColor();


	// Variables ///////////////////////////////////
public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* ColorButton;

	UPROPERTY(BlueprintReadWrite, Category = "Color")
	FDrawingColor DrawingColor;
};
