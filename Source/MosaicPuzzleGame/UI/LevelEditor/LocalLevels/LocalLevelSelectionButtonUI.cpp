// Game created by Bakhali. All right reserved.

#include "LocalLevelSelectionButtonUI.h"
#include "GameManagers/MainGameInstance.h"
#include "Components/Button.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "UI/LevelEditor/LevelEditorUI.h"


ULocalLevelSelectionButtonUI::ULocalLevelSelectionButtonUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void ULocalLevelSelectionButtonUI::NativeConstruct()
{
	Super::NativeConstruct();

	UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameAndUI());
	//UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameOnly());

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULocalLevelSelectionButtonUI > NativeConstruct > GameInstance is nullptr"));
	}


	// Initialize buttons
	if (LocalLevelSelectButton != nullptr) { LocalLevelSelectButton->OnClicked.AddDynamic(this, &ULocalLevelSelectionButtonUI::SelectLevelForEdit); }
	else { UE_LOG(LogTemp, Error, L"ULocalLevelSelectionButtonUI > NativeConstruct > LocalLevelSelectButton is nullptr"); }
}

void ULocalLevelSelectionButtonUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void ULocalLevelSelectionButtonUI::InitializeWidget(ULevelEditorUI * InLevelEditorUI)
{
	LevelEditorUI = InLevelEditorUI;
}

void ULocalLevelSelectionButtonUI::SelectLevelForEdit()
{
	FString LevelName = LocalLevelName.ToString();
	LevelEditorUI->SelectLocalLevelToLoad(LevelName);
}