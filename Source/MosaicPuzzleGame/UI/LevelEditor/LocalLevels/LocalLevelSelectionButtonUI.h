// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LocalLevelSelectionButtonUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API ULocalLevelSelectionButtonUI : public UUserWidget
{
	GENERATED_BODY()
		// Functions //////////////////////////////////
public:
	ULocalLevelSelectionButtonUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void InitializeWidget(class ULevelEditorUI* InLevelEditorUI);

	UFUNCTION()
	void SelectLevelForEdit();

	// Variables //////////////////////////////////
private:
	// Level editor ui
	UPROPERTY()
	class ULevelEditorUI* LevelEditorUI;

public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* LocalLevelSelectButton;
	
	// Text
	UPROPERTY(BlueprintReadWrite, Category = "Textes")
	FText LocalLevelName;

	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;


};
