// Game created by Bakhali. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelEditorUI.generated.h"

/**
 * 
 */
UCLASS()
class MOSAICPUZZLEGAME_API ULevelEditorUI : public UUserWidget
{
	GENERATED_BODY()

	// Functions //////////////////////////////////
public:
	ULevelEditorUI(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct()  override;
	void AddLocalLevelNameButton(FString LocalLevelName);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void AddRow();
	UFUNCTION()
	void SubRow();
	UFUNCTION()
	void AddCol();
	UFUNCTION()
	void SubCol();

	UFUNCTION()
	void UpdateMapInfos();

	UFUNCTION()
	void SaveLevel();

	UFUNCTION()
	void ClearLevel();

	UFUNCTION()
	void SelectLocalLevelToLoad(FString LevelName);
	
	UFUNCTION()
	TArray<FString> LoadLocalLevelsName();

	UFUNCTION()
	void AddNewLocalLevel();

	UFUNCTION(BlueprintCallable, Category = "Levels")
	FText GetLevelNameBeingEdited() const;

	// Variables //////////////////////////////////
public:
	// Buttons
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* RowSubButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* RowAddButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* ColSubButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* ColAddButton;
	
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* SaveButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* ClearButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* EditorMenuButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* EditorNewButton;
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* EditorExitButton;

	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	class UButton* AddNewLocalLevelButton;

	// Text
	UPROPERTY(BlueprintReadWrite, Category = "Textes")
	FText LevelRowNum;
	UPROPERTY(BlueprintReadWrite, Category = "Textes")
	FText LevelColNum;

	// Game instance
	UPROPERTY()
	class UMainGameInstance* GameInstance;

	// Game map creator pointer
	UPROPERTY()
	class AGameMapCreator* GameMapCreator;

	// Color buttons ~~~~~~~~~~~~~~~~~~~~~~
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Editor Color Button")
	TSubclassOf<class UEditorColorButtonUI> EditorColorButtonClass;

	UPROPERTY()
	TArray<class UEditorColorButtonUI*> EditorColorButtonInstancesList;

	// Buttons parent
	UPROPERTY(BlueprintReadWrite, Category = "UMG Editor Color Button")
	class UPanelWidget* ColorButtonParentInstance;

	// Local level selection ~~~~~~~~~~~~~~~~~~~
	UPROPERTY(BlueprintReadWrite, Category = "UMG Local Level Selection")
	class UPanelWidget* LocalLevelSelectionOverlay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG Local Level Selection")
	TSubclassOf<class ULocalLevelSelectionButtonUI> LocalLevelSelectionButtonClass;

	UPROPERTY()
	TArray<class ULocalLevelSelectionButtonUI*> LocalLevelSelectionButtonInstancesList;
	
	// Widget parent
	UPROPERTY(BlueprintReadWrite, Category = "UMG Local Level Selection")
	class UPanelWidget* LocalLevelSelectionButtonParentInstance;
};
