// Game created by Bakhali. All right reserved.

#include "EditorColorButtonUI.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/UMG/Public/Blueprint/WidgetLayoutLibrary.h"
#include "Components/Button.h"
#include "Common/GameStructures.h"
#include "GameManagers/MainGameInstance.h"

UEditorColorButtonUI::UEditorColorButtonUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UEditorColorButtonUI::NativeConstruct()
{
	Super::NativeConstruct();

	if (ColorButton != nullptr) { ColorButton->OnClicked.AddDynamic(this, &UEditorColorButtonUI::ChangeDrawingColor); }
	else { UE_LOG(LogTemp, Error, L"UEditorColorButtonUI > NativeConstruct > ColorButton is nullptr"); }
}

void UEditorColorButtonUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void UEditorColorButtonUI::ApplyColorToButton(FDrawingColor DrawingColorValue)
{
	DrawingColor = DrawingColorValue;
	ColorButton->SetBackgroundColor(DrawingColorValue.Color);
}

void UEditorColorButtonUI::ChangeDrawingColor()
{
	UMainGameInstance* GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	GameInstance->ChangeDrawingColor(this->DrawingColor);
}
