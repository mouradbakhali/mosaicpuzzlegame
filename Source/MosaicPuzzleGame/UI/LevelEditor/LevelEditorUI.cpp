// Game created by Bakhali. All right reserved.

#include "LevelEditorUI.h"
#include "GameManagers/MainGameInstance.h"
#include "Components/Button.h"
#include "Components/HorizontalBoxSlot.h"
#include "GPE/GameMap/GameMapCreator.h"
#include "UI/LevelEditor/EditorColorButtonUI.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "LocalLevels/LocalLevelSelectionButtonUI.h"


ULevelEditorUI::ULevelEditorUI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void ULevelEditorUI::NativeConstruct()
{
	Super::NativeConstruct();

	//UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameAndUI());
	//UGameplayStatics::GetPlayerController(this, 0)->SetInputMode(FInputModeGameOnly());

	// Initialize game instance
	GameInstance = GetWorld()->GetGameInstance<UMainGameInstance>();
	if (GameInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelEditorUI > NativeConstruct > GameInstance is nullptr"));
	}

	// Get game map creator pointer
	if (GameMapCreator == nullptr)
	{
		TArray<AActor*> ActorsList;
		UGameplayStatics::GetAllActorsOfClass(this, AGameMapCreator::StaticClass(), ActorsList);
		if (ActorsList.Num() > 0)
		{
			GameMapCreator = Cast<AGameMapCreator>(ActorsList[0]);
			if (ActorsList.Num() > 1)
			{
				UE_LOG(LogTemp, Warning, TEXT("ULevelEditorUI > NativeConstruct > More than one GameMapCreator in the level !"));
			}
		}
	}

	// Update variables from game map creator
	if (GameMapCreator != nullptr)
	{
		UpdateMapInfos();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("ULevelEditorUI > NativeConstruct > GameMapCreator is nullptr."));
	}

	// Initialize buttons
	if (RowAddButton != nullptr) { RowAddButton->OnClicked.AddDynamic(this, &ULevelEditorUI::AddRow); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > RowAddButton is nullptr"); }

	if (RowSubButton != nullptr) { RowSubButton->OnClicked.AddDynamic(this, &ULevelEditorUI::SubRow); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > RowSubButton is nullptr"); }

	if (ColAddButton != nullptr) { ColAddButton->OnClicked.AddDynamic(this, &ULevelEditorUI::AddCol); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > ColAddButton is nullptr"); }

	if (ColSubButton != nullptr) { ColSubButton->OnClicked.AddDynamic(this, &ULevelEditorUI::SubCol); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > ColSubButton is nullptr"); }
	
	if (SaveButton != nullptr) { SaveButton->OnClicked.AddDynamic(this, &ULevelEditorUI::SaveLevel); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > SaveButton is nullptr"); }

	if (ClearButton != nullptr) { ClearButton->OnClicked.AddDynamic(this, &ULevelEditorUI::ClearLevel); }
	else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > ClearButton is nullptr"); }

	
	if (GameInstance->bLocalLevelsEditingEnabled)
	{
		if (AddNewLocalLevelButton != nullptr) { AddNewLocalLevelButton->OnClicked.AddDynamic(this, &ULevelEditorUI::AddNewLocalLevel); }
		else { UE_LOG(LogTemp, Error, L"ULevelEditorUI > NativeConstruct > AddNewLocalLevelButton is nullptr"); }
	}

	// Create color buttons
	int ColorNum = GameInstance->DrawingColorsList.Num();
	for (int i = 0; i < ColorNum; i++)
	{
		// Create color button
		UEditorColorButtonUI* EditorColorButton = CreateWidget<UEditorColorButtonUI>(this, EditorColorButtonClass);
		UPanelSlot* Slot = ColorButtonParentInstance->AddChild(Cast<UWidget>(EditorColorButton));
		
		// Change button color
		EditorColorButton->ApplyColorToButton(GameInstance->DrawingColorsList[i]);

		// Set slot padding
		Cast<UHorizontalBoxSlot>(Slot)->SetPadding(FMargin(FVector2D(10.0f, 5.0f)));

		// Add it to list
		EditorColorButtonInstancesList.Add(EditorColorButton);
	}

	// Local level selection
	if (!GameInstance->bLocalLevelsEditingEnabled)
	{
		LocalLevelSelectionOverlay->RemoveFromParent();
	}
	else
	{
		// Initialize local level list
		TArray<FString> LocalLevelNamesList = LoadLocalLevelsName();
		if (LocalLevelNamesList.Num() > 0)
		{
			for (int i = 0; i < LocalLevelNamesList.Num(); i++)
			{
				AddLocalLevelNameButton(*LocalLevelNamesList[i]);
			}
		}
	}
}

void ULevelEditorUI::AddLocalLevelNameButton(FString LocalLevelName) 
{
	UE_LOG(LogTemp, Log, TEXT("ULevelEditorUI > AddLocalLevelNameButton > Level name: %s"), *LocalLevelName);
	ULocalLevelSelectionButtonUI* LocalLevelSelectionButton = CreateWidget<ULocalLevelSelectionButtonUI>(this, LocalLevelSelectionButtonClass);
	LocalLevelSelectionButtonParentInstance->AddChild(Cast<UWidget>(LocalLevelSelectionButton));
	LocalLevelSelectionButton->InitializeWidget(this);
	LocalLevelSelectionButton->LocalLevelName = FText::FromString(LocalLevelName);
	LocalLevelSelectionButtonInstancesList.Add(LocalLevelSelectionButton);
}

void ULevelEditorUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

}

void ULevelEditorUI::AddRow()
{
	GameMapCreator->AddLevelRow();
	UpdateMapInfos();
}
void ULevelEditorUI::SubRow()
{
	GameMapCreator->SubLevelRow();
	UpdateMapInfos();
}
void ULevelEditorUI::AddCol()
{
	GameMapCreator->AddLevelCol();
	UpdateMapInfos();
}
void ULevelEditorUI::SubCol()
{
	GameMapCreator->SubLevelCol();
	UpdateMapInfos();
}

void ULevelEditorUI::UpdateMapInfos()
{
	// Set size text
	LevelRowNum = FText::AsNumber(GameMapCreator->LevelDatas.LevelGridRowNum);
	LevelColNum = FText::AsNumber(GameMapCreator->LevelDatas.LevelGridColNum);
}

void ULevelEditorUI::SaveLevel()
{
	GameMapCreator->SaveSelectedLevel();
}

void ULevelEditorUI::ClearLevel()
{
	GameMapCreator->ClearSelectedLevel();
}

void ULevelEditorUI::SelectLocalLevelToLoad(FString LocalLevelName)
{
	GameMapCreator->LoadSelectedLocalLevelForEdit(LocalLevelName);
}

TArray<FString> ULevelEditorUI::LoadLocalLevelsName()
{
	return GameInstance->LoadLocalLevelsName();
}

void ULevelEditorUI::AddNewLocalLevel()
{
	TArray<FString> LocalLevelsNamesList = LoadLocalLevelsName();
	FString NewLocalLevelName;
	if (LocalLevelsNamesList.Num() == 0)
	{
		// Create new level Level_1
		NewLocalLevelName = TEXT("Level_1");
		GameMapCreator->AddNewLocalLevelForEdit(NewLocalLevelName);

	}
	else
	{
		// Get last name and create new one  after it
		FString LastLocalLevelsName = LocalLevelsNamesList[LocalLevelsNamesList.Num() - 1];
		FString LastLevelStringNumber;
		LastLocalLevelsName.Split(TEXT("."), &LastLevelStringNumber, nullptr);
		LastLocalLevelsName.Split(TEXT("_"), nullptr, &LastLevelStringNumber);
		int32 NewLevelNumber = FCString::Atoi(*LastLevelStringNumber) + 1;

		NewLocalLevelName = FString::Printf(TEXT("Level_%d.json"), NewLevelNumber);
		GameMapCreator->AddNewLocalLevelForEdit(NewLocalLevelName);
	}

	AddLocalLevelNameButton(NewLocalLevelName);
}

FText ULevelEditorUI::GetLevelNameBeingEdited() const
{
	FString LevelName;
	GameMapCreator->SelectedForEditLevelName.Split(".", &LevelName, nullptr);
	return FText::FromString(LevelName);
}
